﻿using AutoMapper;
using Core.Services.DTO.Subscription;
using InspireIT.DapperPagination;
using Data.Repositories.DTO.Item;
using Presentation.WebApi.Models.Item;
using Presentation.WebApi.Models.Subscription;

namespace Presentation.WebApi.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            MapSingle();
        }

        private void MapSingle()
        {
            CreateMap<MyCompanySubscriptionsDTO, CompanySubscriptionsJson>().ReverseMap();
            CreateMap<SubscriptionPlanJson, SubscriptionPlanDTO>();
            CreateMap<CompanySubscriptionPaginationJson, PagedList<MyCompanySubscriptionsDTO>>();
            CreateMap<CreateCompanySubscriptionJson, CreateCompanySubscriptionDTO>();
            CreateMap<ExtendCompanySubscriptionJson, ExtendCompanySubscriptionDTO>();
            CreateMap<ExistingSubscriptionActionJson, ExistingSubscriptionActionDTO>();

            CreateMap<AddItemModelJson, ItemDTO>().ForMember(dest => dest.Category, opt => opt.MapFrom(src => new ItemDTO.CategoryDTO{Name = src.CategoryName}) )
                .ForMember(dest => dest.County, opt => opt.MapFrom(src => new ItemDTO.CountyDTO() {Name = src.CountyName}))
                .ForMember(dest => dest.Company, opt => opt.MapFrom(src => new ItemDTO.CompanyDTO() { Name = src.CompanyName }))
                .ForMember(dest => dest.MeasurementUnit, opt => opt.MapFrom(src => new ItemDTO.MeasurementUnitDTO() { Name = src.MeasurementUnitName })).ReverseMap();
        }
    }
}
