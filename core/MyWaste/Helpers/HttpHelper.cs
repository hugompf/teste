﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace Presentation.WebApi.Helpers
{
    public static class HttpHelper
    {
        public static Guid GetAccountId(this IHeaderDictionary header)
        {
            Guid.TryParse(header.GetElement("accountId"), out var accountId);
            return accountId;
        }

        private static string GetElement(this IHeaderDictionary header, string key)
            => !header.TryGetValue(key, out var keys) ? null : keys.First();
    }
}