﻿using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using InspireIT.Notifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.WebApi.Controllers.Subscription
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/invoices")]
    public class InvoiceController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IInvoiceService _invoiceService;
        private readonly NotificationContext _notificationContext;
        public InvoiceController(
            IInvoiceService invoiceService,
            IMapper mapper,
            NotificationContext notificationContext)
        {
            _mapper = mapper;
            _invoiceService = invoiceService;
            _notificationContext = notificationContext;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [Route("{id}")]
        public async Task<IActionResult> GetInvoice(int id)
        {
            var result = await _invoiceService.GetAsync(id);

            return new FileContentResult(result == null ? new byte[0]: result.Content, "application/pdf")
            {
                FileDownloadName = result?.FileName
            };
        }
    }
}
