﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using InspireIT.DapperPagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebApi.Models.Subscription;

namespace Presentation.WebApi.Controllers.Subscription
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/subscription")]
    public class SubscriptionController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ISubscriptionPlanService _subscriptionPlanService;
        private readonly ICompanySubscriptionService _companySubscriptionService;

        public SubscriptionController(
            ICompanySubscriptionService companySubscriptionService,
            IMapper mapper,
            ISubscriptionPlanService subscriptionPlanService)
        {
            _mapper = mapper;
            _companySubscriptionService = companySubscriptionService;
            _subscriptionPlanService = subscriptionPlanService;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("all")]
        public async Task<IActionResult> GetSubscriptionPlans()
        {
            var result = await _subscriptionPlanService.GetAllSubscriptionPlans();
            return Ok(_mapper.Map<List<SubscriptionPlanJson>>(result));
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [Route("my-subscriptions")]
        public async Task<IActionResult> GetMySubscriptions(int pageNumber,  int pageSize, string order, string filter)
        {
            var userLoggedInGuid = new Guid("3D7BC2DC-CC82-4B41-9550-143FD5FD17ED"); //Change when authentication is done
            
            var result = await _companySubscriptionService.GetMySubscriptionsAsync(userLoggedInGuid, new PaginationParameters(pageNumber, pageSize, order, filter));
            var mapped = _mapper.Map<CompanySubscriptionPaginationJson>(result);

            return Ok(mapped);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("subscribe")]
        public async Task<IActionResult> AddCompanyToSubscription([FromBody] CreateCompanySubscriptionJson json)
        {
            var dto = _mapper.Map<CreateCompanySubscriptionDTO>(json);

            var expireDate = await _companySubscriptionService.AddCompanyToSubscription(dto);

            return Ok(expireDate);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("renew")]
        public async Task<IActionResult> RenewSubscription([FromBody] ExistingSubscriptionActionJson json)
        {
            var dto = _mapper.Map<ExistingSubscriptionActionDTO>(json);

            var expireDate = await _companySubscriptionService.RenewSubscription(dto);

            return Ok(expireDate);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("cancel")]
        public async Task<IActionResult> CancelSubscription([FromBody] ExistingSubscriptionActionJson json)
        {
            var dto = _mapper.Map<ExistingSubscriptionActionDTO>(json);

            await _companySubscriptionService.CancelSubscription(dto);
            
            return Ok();
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("extend-subscription")]
        public async Task<IActionResult> ExtendSubscription([FromBody] ExtendCompanySubscriptionJson json)
        {
            var dto = _mapper.Map<ExtendCompanySubscriptionDTO>(json);

            var expireDate = await _companySubscriptionService.ExtendCompanySubscription(dto);

            return Ok(expireDate);
        }
    }
}
