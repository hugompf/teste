﻿using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebApi.Helpers;

namespace Presentation.WebApi.Controllers.Subscription
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/accounts")]
    public class AccountController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService, IMapper mapper)
        {
            _mapper = mapper;
            _accountService = accountService;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAccount()
        {
            var accountIdHeader = HttpContext.Request.Headers.GetAccountId();
            var result = await _accountService.Get(accountIdHeader);
            return Ok(result);
        }
    }
}
