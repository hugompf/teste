﻿using AutoMapper;
using Core.Services.Contracts.Item;
using Core.Services.Contracts.UploadImage;
using Data.Repositories.DTO.Item;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebApi.Models.Item;
using System.Threading.Tasks;

namespace Presentation.WebApi.Controllers.Item
{
    [AllowAnonymous]
    [Route("api/items")]
    public class ItemController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IItemService _itemService;
        private readonly IImageHandler _imageHandler;

        public ItemController(IItemService itemService, IMapper mapper, IImageHandler imageHandler)
        {
            _mapper = mapper;
            _itemService = itemService;
            _imageHandler = imageHandler;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("{id}")]
        public async Task<IActionResult> GetItem(int id)
        {
            var result = await _itemService.GetItem(id);
            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("allItems")]
        public async Task<IActionResult> GetAllItems()
        {
            var result = await _itemService.GetAllItems();
            return Ok(result);
        }

        [HttpPost("addItem")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddItem(AddItemModelJson itemModel)
        {
            var item = _mapper.Map<ItemDTO>(itemModel);
            
            await _itemService.AddItemAsync(item);
            
            return Ok();
        }
    }
}