﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Presentation.WebApi.Models.Subscription;

namespace Core.Services.DTO.Subscription
{
    public class CompanySubscriptionPaginationJson
    {
        [JsonProperty("rows_per_page")]
        public int RowsPerPage { get; set; }

        [JsonProperty("total_rows")]
        public int? TotalRows { get; set; }

        [JsonProperty("total_pages")]
        public int? TotalPages { get; set; }

        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("previous_page")]
        public int? PreviousPage { get; set; }

        [JsonProperty("next_page")]
        public int? NextPage { get; set; }

        [JsonProperty("has_previous")]
        public bool HasPrevious => (CurrentPage > 1);

        [JsonProperty("has_next")]
        public bool HasNext => (TotalPages.HasValue && (CurrentPage < TotalPages.Value));

        [JsonProperty("items")]
        public IEnumerable<CompanySubscriptionsJson> Items { get; set; }
    }
}
