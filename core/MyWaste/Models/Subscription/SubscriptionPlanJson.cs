﻿using Newtonsoft.Json;

namespace Presentation.WebApi.Models.Subscription
{
    public class SubscriptionPlanJson
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("monthly_contact_request_limit")]
        public int MonthlyContactRequestLimit { get; set; }

        [JsonProperty("monthly_item_highlight_limit")]
        public int MonthlyItemHighlightLimit { get; set; }

        [JsonProperty("notifications_available")]
        public bool NotificationsAvailable { get; set; }
    }
}
