﻿using System;
using Newtonsoft.Json;

namespace Presentation.WebApi.Models.Subscription
{
    public class ExtendCompanySubscriptionJson
    {
        [JsonProperty("duration_extension")]
        public int DurationExtension { get; set; }

        [JsonProperty("account_id")]
        public Guid AccountId { get; set; }

        [JsonProperty("company_subscription_id")]
        public int CompanySubscriptionId { get; set; }
    }
}
