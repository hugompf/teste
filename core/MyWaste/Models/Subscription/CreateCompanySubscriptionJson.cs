﻿using System;
using Newtonsoft.Json;

namespace Presentation.WebApi.Models.Subscription
{
    public class CreateCompanySubscriptionJson
    {
        [JsonProperty("account_id")]
        public Guid AccountId { get; set; }

        [JsonProperty("subscription_plan_id")]
        public int SubscriptionPlanId { get; set; }

        [JsonProperty("microsite_id")]
        public int MicrositeId { get; set; }
    }
}
