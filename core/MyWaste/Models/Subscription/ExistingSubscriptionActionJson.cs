﻿using System;
using Newtonsoft.Json;

namespace Presentation.WebApi.Models.Subscription
{
    public class ExistingSubscriptionActionJson
    {
        [JsonProperty("account_id")]
        public Guid AccountId { get; set; }

        [JsonProperty("subscription_id")]
        public int SubscriptionId { get; set; }
    }
}
