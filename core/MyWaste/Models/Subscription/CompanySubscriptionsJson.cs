﻿using System;
using Newtonsoft.Json;

namespace Presentation.WebApi.Models.Subscription
{
    public class CompanySubscriptionsJson
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("end_date")]
        public DateTime EndDate { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("plan_name")]
        public string PlanName { get; set; }

        [JsonProperty("invoice_id")]
        public int InvoiceId { get; set; } 
    }
}
