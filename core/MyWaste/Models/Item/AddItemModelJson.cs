﻿using Newtonsoft.Json;
using System;
using Microsoft.AspNetCore.Http;

namespace Presentation.WebApi.Models.Item
{
    public class AddItemModelJson
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("registration_date")]
        public DateTime RegistrationDate { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("highligthed")]
        public bool Highligthed { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("county_name")]
        public string CountyName { get; set; }

        [JsonProperty("company_name")]
        public string CompanyName { get; set; }

        [JsonProperty("measurement_unit_name")]
        public string MeasurementUnitName { get; set; }

        [JsonProperty("item_image")]
        public IFormFile ItemImage { get; set; }
    }
}