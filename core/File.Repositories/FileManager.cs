﻿using Domain.Entities;
using File.Repositories.Contracts;
using File.Repositories.DTO;
using File.Repositories.UploadActions;
using InspireIT.Notifications;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;

namespace File.Repositories
{
    public class FileManager : IFileManager
    {
        private Dictionary<FileTypes, IDoFileAction> UploadStrategy { get; }


        public FileManager(IConfiguration config, NotificationContext notificationContext)
        {
            UploadStrategy = new Dictionary<FileTypes, IDoFileAction>
            {
                { FileTypes.Invoice, new FileSystem(config, notificationContext) }
            };
        }

        public FileDTO Load(LoadFileModel file)
        {
            var bytes = UploadStrategy[file.Type].Load(file.Path);
            var extension = Path.GetExtension(file.Path);
            var name = Path.GetFileName(file.Path);

            return new FileDTO()
            {
                Extension = extension,
                FileName = name,
                Content = bytes
            };
        }
    }
}