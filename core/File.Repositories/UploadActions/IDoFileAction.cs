﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace File.Repositories.UploadActions
{
    public interface IDoFileAction
    {
        Task<string> UploadImage(IFormFile file);

        byte[] Load(string path);
    }
}
