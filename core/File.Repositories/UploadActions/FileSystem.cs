﻿using System;
using System.IO;
using InspireIT.Notifications;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace File.Repositories.UploadActions
{
    public class FileSystem : IDoFileAction
    {
        private readonly string _path;
        private readonly string _pathImage;
        private readonly NotificationContext _notificationContext;

        public FileSystem(IConfiguration config, NotificationContext notificationContext)
        {
            _path = config.GetConnectionString("FileSystemRepositoryString");
            _pathImage = config.GetConnectionString("ImageFileSystemRepository");
            _notificationContext = notificationContext;
        }

        public byte[] Load(string path)
        {
            if (path == null) return null;

            var absolutePath = _path + path;

            if (System.IO.File.Exists(absolutePath))
                return System.IO.File.ReadAllBytes(absolutePath);

            _notificationContext.AddNotification("Error", "A fatura não está disponivel neste momento.");
            return null;
        }

        public async Task<string> UploadImage(IFormFile file)
        {
            if (CheckIfIsImageFile(file))
            {
                return await WriteFile(file);
            }

            return "Invalid image file";
        }

        private bool CheckIfIsImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                fileBytes = memoryStream.ToArray();
            }

            return ItemImage.GetImageFormat(fileBytes) != ItemImage.ImageFormat.unknown;
        }

        public async Task<string> WriteFile(IFormFile file)
        {
            string fileName;
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = Guid.NewGuid().ToString() + extension;
                var path = Path.Combine(Directory.GetCurrentDirectory(), _pathImage, fileName);

                using (var bits = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(bits);
                }

                return path;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}


