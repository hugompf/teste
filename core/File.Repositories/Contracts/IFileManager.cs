﻿using Domain.Entities;
using File.Repositories.DTO;

namespace File.Repositories.Contracts
{
    public interface IFileManager
    {
        FileDTO Load(LoadFileModel file);
    }
}
