﻿
namespace File.Repositories.DTO
{
    public class FileDTO
    {
        public string FileName { get; set; }
        public string Extension { get; set; }
        public byte[] Content { get; set; }
    }
}
