﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Core.Services.Contracts.UploadImage
{
    public interface IImageHandler
    {
        Task<string> UploadImage(IFormFile file);
    }
}
