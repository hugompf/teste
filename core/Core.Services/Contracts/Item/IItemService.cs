﻿using Data.Repositories.DTO.Item;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Services.Contracts.Item
{
    public interface IItemService
    {
        Task<ItemDTO> GetItem(int id);

        Task AddItemAsync(ItemDTO dto);

        Task<IEnumerable<ItemDTO>> GetAllItems();
    }
}
