﻿using System;
using System.Threading.Tasks;
using Core.Services.DTO.Subscription;
using InspireIT.DapperPagination;
using Data.Repositories.DTO.SubscriptionDTO;

namespace Core.Services.Contracts.Subscription
{
    public interface ICompanySubscriptionService
    {
        Task<CompanySubscriptionDTO> Get(int id);

        Task<DateTime?> AddCompanyToSubscription(CreateCompanySubscriptionDTO companySubscription);

        Task<PagedList<MyCompanySubscriptionsDTO>> GetMySubscriptionsAsync(Guid accountId, PaginationParameters paginationParameters);

        Task<DateTime?> ExtendCompanySubscription(ExtendCompanySubscriptionDTO companySubscription);

        Task<object> RenewSubscription(ExistingSubscriptionActionDTO companySubscription);

        Task<object> CancelSubscription(ExistingSubscriptionActionDTO companySubscription);
    }
}
