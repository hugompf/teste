﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Services.DTO.Subscription;

namespace Core.Services.Contracts.Subscription
{
    public interface ISubscriptionPlanService
    {
        Task<IEnumerable<SubscriptionPlanDTO>> GetAllSubscriptionPlans();
    }
}
