﻿using Core.Services.DTO.Subscription;
using System;
using System.Threading.Tasks;

namespace Core.Services.Contracts.Subscription
{
    public interface IAccountService
    {
        Task<AccountNamingDTO> Get(Guid id);
    }
}
