﻿using System.Threading.Tasks;
using Core.Services.DTO.Subscription;

namespace Core.Services.Contracts.Subscription
{
    public interface IInvoiceService
    {
        Task<InvoiceFileDTO> GetAsync(int id);
    }
}
