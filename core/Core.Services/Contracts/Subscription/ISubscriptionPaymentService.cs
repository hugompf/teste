﻿using System.Threading.Tasks;
using Core.Services.DTO.Subscription;

namespace Core.Services.Contracts.Subscription
{
    public interface ISubscriptionPaymentService
    {
        Task Create(SubscriptionPaymentDTO subscriptionPayment);
    }
}
