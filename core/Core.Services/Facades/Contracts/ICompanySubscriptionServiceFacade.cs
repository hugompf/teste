﻿using System;
using System.Threading.Tasks;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.DapperPagination;

namespace Core.Services.Facades.Contracts
{
    public interface ICompanySubscriptionServiceFacade
    {
        Task<CompanySubscription> GetCompanySubscriptionAsync(object id);
        Task<object> AddCompanySubscriptionAsync(CompanySubscription entity);
        Task<object> UpdateCompanySubscriptionAsync(CompanySubscription obj);

        Task<object> RenewSubscriptionAsync(CompanySubscription companySubscription);
        Task<object> CancelSubscriptionAsync(CompanySubscription companySubscription);

        Task<CompanySubscriptionDTO> GetNonFreeActiveSubscriptionByMicrosite(int companyId, int micrositeId);
        Task<CompanySubscription> GetCompanyActiveFreeSubscriptionByMicrosite(int companyId, int micrositeId);
        Task<PagedList<CompanySubscriptionDTO>> GetMySubscriptionsAsync(Guid id, PaginationParameters paginationParameters);

        AccountDTO GetAccountWithEmail(Guid id);
        Task<Account> GetAccountAsync(Guid id);

        Task<SubscriptionPlan> GetSubscriptionPlanAsync(object id);
    }
}
