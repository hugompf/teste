﻿using System;
using System.Threading.Tasks;
using Core.Services.Facades.Contracts;
using Data.Repositories.Contracts.Subscription;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.DapperPagination;

namespace Core.Services.Facades.Implementations
{
    public class CompanySubscriptionServiceFacade : ICompanySubscriptionServiceFacade
    {
        private readonly ICompanySubscriptionRepository _companySubscriptionRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly ISubscriptionPlanRepository _subscriptionPlanRepository;

        public CompanySubscriptionServiceFacade(
            ICompanySubscriptionRepository companySubscriptionRepository,
            IAccountRepository accountRepository,
            ISubscriptionPlanRepository subscriptionPlanRepository)
        {
            _companySubscriptionRepository = companySubscriptionRepository;
            _accountRepository = accountRepository;
            _subscriptionPlanRepository = subscriptionPlanRepository;
        }

        public async Task<CompanySubscription> GetCompanySubscriptionAsync(object id)
        {
            return await _companySubscriptionRepository.GetAsync(id);
        }

        public async Task<object> AddCompanySubscriptionAsync(CompanySubscription entity)
        {
            return await _companySubscriptionRepository.AddAsync(entity);
        }

        public async Task<object> UpdateCompanySubscriptionAsync(CompanySubscription obj)
        {
            return await _companySubscriptionRepository.UpdateAsync(obj);
        }

        public async Task<object> RenewSubscriptionAsync(CompanySubscription companySubscription)
        {
            return await _companySubscriptionRepository.RenewSubscriptionAsync(companySubscription);
        }

        public async Task<object> CancelSubscriptionAsync(CompanySubscription companySubscription)
        {
            return await _companySubscriptionRepository.CancelSubscriptionAsync(companySubscription);
        }

        public async Task<CompanySubscriptionDTO> GetNonFreeActiveSubscriptionByMicrosite(int companyId, int micrositeId)
        {
            return await _companySubscriptionRepository.GetNonFreeActiveSubscriptionByMicrosite(companyId, micrositeId);
        }

        public async Task<CompanySubscription> GetCompanyActiveFreeSubscriptionByMicrosite(int companyId, int micrositeId)
        {
            return await _companySubscriptionRepository.GetCompanyActiveFreeSubscriptionByMicrosite(companyId,
                micrositeId);
        }

        public async Task<PagedList<CompanySubscriptionDTO>> GetMySubscriptionsAsync(Guid id,PaginationParameters paginationParameters)
        {
            return await _companySubscriptionRepository.GetMySubscriptionsAsync(id, paginationParameters);
        }

        public AccountDTO GetAccountWithEmail(Guid id)
        {
            return _accountRepository.GetAccountWithEmail(id);
        }

        public async Task<Account> GetAccountAsync(Guid id)
        {
            return await _accountRepository.GetAsync(id);
        }

        public async Task<SubscriptionPlan> GetSubscriptionPlanAsync(object id)
        {
            return await _subscriptionPlanRepository.GetAsync(id);
        }
    }
}
