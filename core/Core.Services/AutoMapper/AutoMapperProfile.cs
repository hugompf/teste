﻿using AutoMapper;
using Core.Services.DTO.Subscription;
using Data.Repositories.DTO.Item;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Item;
using Domain.Entities.Subscription;
using InspireIT.DapperPagination;

namespace Core.Services.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            DefaultMapping();
        }

        private void DefaultMapping()
        {
            CreateMap<AccountNamingDTO, Account>();

            CreateMap<CreateCompanySubscriptionDTO, CompanySubscription>();

            CreateMap<CompanySubscription, CompanySubscriptionDTO>().ReverseMap();
            
            CreateMap<SubscriptionPaymentDTO, SubscriptionPayment>();

            CreateMap<CompanySubscriptionDTO, MyCompanySubscriptionsDTO>();

            CreateMap<SubscriptionPlan, SubscriptionPlanDTO>();

            CreateMap<Item, ItemDTO>().ReverseMap();

            CreateMap<ItemDTO.CategoryDTO, Category>().ReverseMap();

            CreateMap<ItemDTO, GetItemDTO>().ReverseMap();

            CreateMap<ItemDTO, GetFKItemDTO>().ReverseMap();

            CreateMap<ItemDTO, ItemDTO.CategoryDTO>().ReverseMap();

            CreateMap<ItemDTO, ItemDTO.CountyDTO>().ReverseMap();

            CreateMap<ItemDTO, ItemDTO.MeasurementUnitDTO>().ReverseMap();

            CreateMap<Category, ItemDTO.CategoryDTO>().ReverseMap();

            CreateMap<County, ItemDTO.CountyDTO>().ReverseMap();

            CreateMap<MeasurementUnit, ItemDTO.MeasurementUnitDTO>().ReverseMap();
            
            CreateMap<Invoice, InvoiceFileDTO>();

            CreateMap<PagedList<CompanySubscriptionDTO>, PagedList<MyCompanySubscriptionsDTO>>();
        }
    }
}
