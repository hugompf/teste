﻿namespace Core.Services.DTO
{
    public class MicrositeDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public CategoryDTO Category { get; set; }

        public class CategoryDTO
        {
            public string Name { get; set; }
        }
    }
}
