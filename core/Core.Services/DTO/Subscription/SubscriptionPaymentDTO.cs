﻿using System;

namespace Core.Services.DTO.Subscription
{
    public class SubscriptionPaymentDTO
    {
        public Guid AccountId { get; set; }

        public int SubscriptionId { get; set; }
    }
}
