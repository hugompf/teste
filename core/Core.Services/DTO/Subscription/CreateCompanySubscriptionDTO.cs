﻿using System;

namespace Core.Services.DTO.Subscription
{
    public class CreateCompanySubscriptionDTO
    {
        public Guid AccountId { get; set; }
        
        public int SubscriptionPlanId { get; set; }

        public int MicrositeId { get; set; }
    }
}
