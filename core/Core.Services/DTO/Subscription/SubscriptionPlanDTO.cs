﻿namespace Core.Services.DTO.Subscription
{
    public class SubscriptionPlanDTO
    {
        public string Name { get; set; }
        
        public int Duration { get; set; }
        
        public double Price { get; set; }
        
        public int MonthlyContactRequestLimit { get; set; }
        
        public int MonthlyItemHighlightLimit { get; set; }

        public bool NotificationsAvailable { get; set; }
    }
}
