﻿using System;

namespace Core.Services.DTO.Subscription
{
    public class AccountNamingDTO
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
