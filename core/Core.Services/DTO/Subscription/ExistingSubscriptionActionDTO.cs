﻿using System;

namespace Core.Services.DTO.Subscription
{
    public class ExistingSubscriptionActionDTO
    {
        public Guid AccountId { get; set; }
        public int SubscriptionId { get; set; }
    }
}
