﻿using System;

namespace Core.Services.DTO.Subscription
{
    public class ExtendCompanySubscriptionDTO
    {
        public int DurationExtension { get; set; }

        public Guid AccountId { get; set; }

        public int CompanySubscriptionId { get; set; }
    }
}
