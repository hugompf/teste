﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Data.Repositories.Contracts.Subscription;
using Domain.Entities.Subscription;

namespace Core.Services.Implementations.Subscription
{
    public class SubscriptionPlanService : ISubscriptionPlanService
    {
        private readonly IMapper _mapper;
        private readonly ISubscriptionPlanRepository _subscriptionPlanRepository;

        public SubscriptionPlanService(
            IMapper mapper,
            ISubscriptionPlanRepository subscriptionPlanRepository)
        {
            _mapper = mapper;
            _subscriptionPlanRepository = subscriptionPlanRepository;
        }

        public async Task<IEnumerable<SubscriptionPlanDTO>> GetAllSubscriptionPlans()
        {
            var subscriptionPlans = await _subscriptionPlanRepository.GetAllSubscriptionPlansAsync();

            return _mapper.Map<IEnumerable<SubscriptionPlan>, IEnumerable<SubscriptionPlanDTO>>(subscriptionPlans);
        }
    }
}