﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Data.Repositories.Contracts.Subscription;

namespace Core.Services.Implementations.Subscription
{
    public class AccountService : IAccountService
    {
        private readonly IMapper _mapper;
        private readonly IAccountRepository _accountRepository;

        public AccountService(
            IMapper mapper,
            IAccountRepository accountRepository)
        {
            _mapper = mapper;
            _accountRepository = accountRepository;
        }

        public async Task<AccountNamingDTO> Get(Guid id)
        {
            var account = await _accountRepository.GetAsync(id);

            return _mapper.Map<AccountNamingDTO>(account);
        }
    }
}
