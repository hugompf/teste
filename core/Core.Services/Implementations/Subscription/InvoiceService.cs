﻿using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Data.Repositories.Contracts.Subscription;
using Domain.Entities;
using File.Repositories.Contracts;
using InspireIT.Notifications;

namespace Core.Services.Implementations.Subscription
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IFileManager _fileManager;
        private readonly IMapper _mapper;
        private readonly NotificationContext _notificationContext;

        public InvoiceService(
            IMapper mapper,
            IInvoiceRepository invoiceRepository,
            IFileManager fileManager,
            NotificationContext notificationContext)
        {
            _mapper = mapper;
            _invoiceRepository = invoiceRepository;
            _fileManager = fileManager;
            _notificationContext = notificationContext;
        }

        public async Task<InvoiceFileDTO> GetAsync(int id)
        {
            var invoice = await _invoiceRepository.GetAsync(id);
            if (invoice is null)
            {
                _notificationContext.AddNotification("Invoice","A fatura não existe no sistema.");
                return null;
            }

            var file = new LoadFileModel
            {
                Type = FileTypes.Invoice,
                Path = invoice.FilePath
            };

            var loadedFile = _fileManager.Load(file);

            return _mapper.Map<InvoiceFileDTO>(loadedFile); 
        }
    }
}
