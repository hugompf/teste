﻿using System.Threading.Tasks;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Data.Repositories.Contracts.Subscription;
using Domain.Entities.Subscription;

namespace Core.Services.Implementations.Subscription
{
    public class SubscriptionPaymentService : ISubscriptionPaymentService
    {
        private readonly IMapper _mapper;
        private readonly ISubscriptionPaymentRepository _subscriptionPaymentRepository;

        public SubscriptionPaymentService(
            IMapper mapper,
            ISubscriptionPaymentRepository subscriptionPaymentRepository)
        {
            _mapper = mapper;
            _subscriptionPaymentRepository = subscriptionPaymentRepository;
        }

        public async Task Create(SubscriptionPaymentDTO subscriptionPayment)
        {
            var newSubscriptionPayment = _mapper.Map<SubscriptionPayment>(subscriptionPayment);
            newSubscriptionPayment.SetCurrentDate();

            await _subscriptionPaymentRepository.AddAsync(newSubscriptionPayment);
        }
    }
}