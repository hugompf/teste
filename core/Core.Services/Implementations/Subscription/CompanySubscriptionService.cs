﻿using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Core.Services.EmailService;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.Notifications;
using System;
using System.Threading.Tasks;
using InspireIT.DapperPagination;
using Core.Services.Facades.Contracts;
using Core.Services.SubscriptionActions.Actions;

namespace Core.Services.Implementations.Subscription
{
    public class CompanySubscriptionService : ICompanySubscriptionService
    {
        private readonly IMapper _mapper;
        private readonly ICompanySubscriptionServiceFacade _companySubscriptionServiceFacade;
        private readonly IEmailSender _emailSender;

        private readonly NotificationContext _notificationContext;

        public CompanySubscriptionService(
            IMapper mapper,
            ICompanySubscriptionServiceFacade companySubscriptionServiceFacade,
            NotificationContext notificationContext,
            IEmailSender emailSender)
        {
            _mapper = mapper;
            _companySubscriptionServiceFacade = companySubscriptionServiceFacade;
            _emailSender = emailSender;

            _notificationContext = notificationContext;
        }

        public async Task<CompanySubscriptionDTO> Get(int id)
        {
            var companySubscription = await _companySubscriptionServiceFacade.GetCompanySubscriptionAsync(id);

            return _mapper.Map<CompanySubscriptionDTO>(companySubscription);
        }

        public async Task<DateTime?> AddCompanyToSubscription(CreateCompanySubscriptionDTO companySubscription)
        {
            var account = _companySubscriptionServiceFacade.GetAccountWithEmail(companySubscription.AccountId);

            if (!await ValidateAddCompanyToSubscription(account, companySubscription.MicrositeId))
            {
                return null;
            }

            // Proceed with subscription
            await EndFreeSubscriptionAsync(account, companySubscription.MicrositeId);

            var subscriptionPlan = await _companySubscriptionServiceFacade.GetSubscriptionPlanAsync(companySubscription.SubscriptionPlanId);

            var newCompanySubscription = _mapper.Map<CompanySubscription>(companySubscription);
            newCompanySubscription.StartSubscription(
                subscriptionPlan.Duration,
                account.CompanyId,
                companySubscription.SubscriptionPlanId,
                companySubscription.MicrositeId
            );

            newCompanySubscription.SubscriptionPayment = BuildSubscriptionPayment(account);

            var result = await _companySubscriptionServiceFacade.AddCompanySubscriptionAsync(newCompanySubscription);

            if (result == null)
            {
                _notificationContext.AddNotification("Error", "Something went wrong");
                return null;
            }

            SendSubscriptionPaymentEmail(account.Email, account.FirstName, subscriptionPlan.Name);

            return newCompanySubscription.EndDate;
        }

        #region AddCompanyToSubscription helpers

        private async Task<bool> ValidateAddCompanyToSubscription(AccountDTO account, int micrositeId)
        {
            if (account == null)
            {
                _notificationContext.AddNotification("Error", "The account is invalid.");
                return false;
            }

            if (await AccountHasActiveNonFreeSubscriptionAsync(account, micrositeId))
            {
                _notificationContext.AddNotification("Error", "There is already an active subscription for this company.");
                return false;
            }

            return true;
        }

        private async Task<bool> AccountHasActiveNonFreeSubscriptionAsync(AccountDTO account, int micrositeId)
        {
            var subscription = await _companySubscriptionServiceFacade.GetNonFreeActiveSubscriptionByMicrosite(account.CompanyId, micrositeId);

            return subscription != null;
        }

        private async Task EndFreeSubscriptionAsync(AccountDTO account, int micrositeId)
        {
            var subscription = await _companySubscriptionServiceFacade.GetCompanyActiveFreeSubscriptionByMicrosite(account.CompanyId, micrositeId);

            if (subscription == null)
            {
                return;
            }

            subscription.EndSubscription();

            await _companySubscriptionServiceFacade.UpdateCompanySubscriptionAsync(subscription);
        }

        private SubscriptionPayment BuildSubscriptionPayment(AccountDTO account)
        {
            var subscriptionPaymentDTO = new SubscriptionPaymentDTO
            {
                AccountId = account.Id
            };

            var subscriptionPayment = _mapper.Map<SubscriptionPayment>(subscriptionPaymentDTO);
            subscriptionPayment.SetCurrentDate();

            return subscriptionPayment;
        }

        private void SendSubscriptionPaymentEmail(string email, string username, string planName)
        {
            _emailSender.SendPaymentConfirmationEmail(email, username, planName);
        }

        #endregion
       
        public async Task<object> RenewSubscription(ExistingSubscriptionActionDTO companySubscription)
        {
            var renew = new RenewSubscription(_mapper, _companySubscriptionServiceFacade, _notificationContext, _emailSender);

            return await renew.Action(companySubscription);
        }

        public async Task<object> CancelSubscription(ExistingSubscriptionActionDTO companySubscription)
        {
            var cancel = new CancelSubscription(_mapper, _companySubscriptionServiceFacade, _notificationContext, _emailSender);

            return await cancel.Action(companySubscription);
        }

        public async Task<DateTime?> ExtendCompanySubscription(ExtendCompanySubscriptionDTO companySubscription)
        {
            await ValidateExtensionRequest(companySubscription);

            if (_notificationContext.HasNotifications)
            {
                return null;
            }

            //TODO 2: Update
            //TODO 3: Send confirmation email (new template)

            return DateTime.MaxValue;
        }

        private async Task ValidateExtensionRequest(ExtendCompanySubscriptionDTO companySubscription)
        {
            //Check if companySubscription exists (non free)
            var existingCompanySubscription = await _companySubscriptionServiceFacade.GetCompanySubscriptionAsync(companySubscription.CompanySubscriptionId);

            if (existingCompanySubscription == null)
            {
                _notificationContext.AddNotification("Error", "That subscription doesn't exist.");
                return;
            }

            //Check if companySubscription is active
            if (!existingCompanySubscription.Active)
            {
                _notificationContext.AddNotification("Error", "That subscription isn't active.");
                return;
            }

            //Check if original duration is smaller or equal than duration being sent
            if (existingCompanySubscription.DurationInDays() >= companySubscription.DurationExtension)
            {
                _notificationContext.AddNotification("Error", "The new subscription duration is smaller or equal to the existing one.");
                return;
            }

            //Check if account requesting belongs to company
            var account = await _companySubscriptionServiceFacade.GetAccountAsync(companySubscription.AccountId);

            if (account == null)
            {
                _notificationContext.AddNotification("Error", "That account doesn't exist.");
                return;
            }

            if (account.CompanyId != existingCompanySubscription.CompanyId)
            {
                _notificationContext.AddNotification("Error", "Account doesn't belong to the company.");
            }
        }

        public async Task<PagedList<MyCompanySubscriptionsDTO>> GetMySubscriptionsAsync(Guid accountId, PaginationParameters paginationParameters)
        {
            var subscriptions = await _companySubscriptionServiceFacade.GetMySubscriptionsAsync(accountId, paginationParameters);
            return _mapper.Map<PagedList<MyCompanySubscriptionsDTO>>(subscriptions);
        }
    }
}
