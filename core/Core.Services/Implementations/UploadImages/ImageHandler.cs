﻿using Core.Services.Contracts.UploadImage;
using File.Repositories.UploadActions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Core.Services.Implementations.UploadImages
{
    public class ImageHandler : IImageHandler
    {
        private readonly IDoFileAction _doFileAction;

        public ImageHandler(IDoFileAction doFileAction)
        {
            _doFileAction = doFileAction;
        }

        public async Task<string> UploadImage(IFormFile file)
        {
            return await _doFileAction.UploadImage(file);
        }
    }
}
