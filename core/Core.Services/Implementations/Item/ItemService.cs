﻿using AutoMapper;
using Core.Services.Contracts.Item;
using Data.Repositories.Contracts.Item;
using Data.Repositories.DTO.Item;
using InspireIT.Notifications;
using System.Collections.Generic;
using System.Threading.Tasks;
using Product = Domain.Entities.Item.Item;

namespace Core.Services.Implementations.Item
{
    public class ItemService : IItemService
    {
        private readonly IMapper _mapper;
        private readonly IItemRepository _itemRepository;
        private readonly NotificationContext _notificationContext;

        public ItemService(IMapper mapper, 
            IItemRepository itemRepository,
            NotificationContext notificationContext)
        {
            _mapper = mapper;
            _itemRepository = itemRepository;
            _notificationContext = notificationContext;
        }

        public async Task<ItemDTO> GetItem(int id)
        {
            var item = await _itemRepository.GetItemInformation(id);
            if (item == null)
            {
                _notificationContext.AddNotification("Error", "Something went wrong, verify that the item exists");
                return null;
            }

            return _mapper.Map<ItemDTO>(item);
        }

        public async Task<IEnumerable<ItemDTO>> GetAllItems()
        {
            var items = await _itemRepository.GetAllItemsAsync();

            return _mapper.Map<IEnumerable<ItemDTO>>(items);
        }

        public async Task AddItemAsync(ItemDTO item)
        {
            var newItem = _mapper.Map<Product>(item);

            newItem.CreateNewItem();

            var itemFk = await _itemRepository.GetCategoryCountyCompanyMeasurementUnitId(item.Category.Name, item.County.Name, item.Company.Name, item.MeasurementUnit.Name);
            if (itemFk == null)
            {
                _notificationContext.AddNotification("Error", "Something went wrong, make sure all fields are filled in correctly");
                return;
            }

            newItem.CategoryId = itemFk.CategoryId;
            newItem.CountyId = itemFk.CountyId;
            newItem.CompanyId = itemFk.CompanyId;
            newItem.MeasurementUnitId = itemFk.MeasurementUnitId;

            var itemIdToFile = await _itemRepository.AddItemIdToFileAsync(newItem, item.ItemImage);
            if (itemIdToFile == null)
            {
                _notificationContext.AddNotification("Error", "Something went wrong");
                return;
            }
        }
    }
}