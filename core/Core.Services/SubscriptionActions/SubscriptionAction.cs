﻿using System.Threading.Tasks;
using AutoMapper;
using Core.Services.DTO.Subscription;
using Core.Services.EmailService;
using Core.Services.Facades.Contracts;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.Notifications;

namespace Core.Services.SubscriptionActions
{
    public abstract class SubscriptionAction
    {
        protected readonly IMapper Mapper;
        protected readonly ICompanySubscriptionServiceFacade CompanySubscriptionServiceFacade;

        protected readonly NotificationContext NotificationContext;

        protected readonly IEmailSender EmailSender;

        protected SubscriptionAction(
            IMapper mapper,
            ICompanySubscriptionServiceFacade companySubscriptionServiceFacade,
            NotificationContext notificationContext,
            IEmailSender emailSender)
        {
            Mapper = mapper;
            CompanySubscriptionServiceFacade = companySubscriptionServiceFacade;

            NotificationContext = notificationContext;

            EmailSender = emailSender;
        }

        public abstract Task<object> SpecificAction(CompanySubscription currentSubscription, AccountDTO account, SubscriptionPlan plan);
        public abstract object SendEmailAndReturnValue(CompanySubscription currentSubscription, AccountDTO account, SubscriptionPlan plan);

        public async Task<object> Action(ExistingSubscriptionActionDTO companySubscription)
        {
            var account = CompanySubscriptionServiceFacade.GetAccountWithEmail(companySubscription.AccountId);

            var currentSubscription = await CompanySubscriptionServiceFacade.GetCompanySubscriptionAsync(companySubscription.SubscriptionId);

            if (!ValidateExistingSubscriptionAction(account, currentSubscription))
            {
                return null;
            } 

            var subscriptionPlan = await CompanySubscriptionServiceFacade.GetSubscriptionPlanAsync(currentSubscription.SubscriptionPlanId);

            var result = await SpecificAction(currentSubscription, account, subscriptionPlan);

            if (result == null)
            {
                NotificationContext.AddNotification("Error", "Something went wrong");
                return null;
            }

            return SendEmailAndReturnValue(currentSubscription, account, subscriptionPlan);
        }

        private bool ValidateExistingSubscriptionAction(AccountDTO account, CompanySubscription currentSubscription)
        {
            if (account == null)
            {
                NotificationContext.AddNotification("Error", "The account is invalid.");
                return false;
            }

            if (currentSubscription == null)
            {
                NotificationContext.AddNotification("Error", "Subscription doesn't exist.");
                return false;
            }

            if (account.CompanyId != currentSubscription.CompanyId)
            {
                NotificationContext.AddNotification("Error", "Account doesn't belong to company.");
                return false;
            }

            if (!currentSubscription.Active)
            {
                NotificationContext.AddNotification("Error", "Subscription isn't active.");
                return false;
            }

            return true;
        }

        protected SubscriptionPayment BuildSubscriptionPayment(AccountDTO account)
        {
            var subscriptionPaymentDTO = new SubscriptionPaymentDTO
            {
                AccountId = account.Id
            };

            var subscriptionPayment = Mapper.Map<SubscriptionPayment>(subscriptionPaymentDTO);
            subscriptionPayment.SetCurrentDate();

            return subscriptionPayment;
        }
    }
}
