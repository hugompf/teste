﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Core.Services.EmailService;
using Core.Services.Facades.Contracts;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.Notifications;

namespace Core.Services.SubscriptionActions.Actions
{
    public class RenewSubscription : SubscriptionAction
    {
        public RenewSubscription(
            IMapper mapper,
            ICompanySubscriptionServiceFacade companySubscriptionServiceFacade, 
            NotificationContext notificationContext, 
            IEmailSender emailSender) : base(mapper, companySubscriptionServiceFacade, notificationContext, emailSender)
        {
        }

        public override async Task<object> SpecificAction(CompanySubscription currentSubscription, AccountDTO account, SubscriptionPlan plan)
        {
            currentSubscription.Renew(plan.Duration);

            currentSubscription.SubscriptionPayment = BuildSubscriptionPayment(account);

            return await CompanySubscriptionServiceFacade.RenewSubscriptionAsync(currentSubscription);
        }

        public override object SendEmailAndReturnValue(CompanySubscription currentSubscription, AccountDTO account, SubscriptionPlan plan)
        {
            SendRenewalEmail(account, plan.Name, currentSubscription.EndDate);

            return currentSubscription.EndDate;
        }

        private void SendRenewalEmail(AccountDTO account, string planName, DateTime endDate)
        {
            EmailSender.SendRenewalConfirmationEmail(account.Email, account.FirstName, planName, endDate.ToShortDateString());
        }
    }
}
