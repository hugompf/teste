﻿using System.Threading.Tasks;
using AutoMapper;
using Core.Services.EmailService;
using Core.Services.Facades.Contracts;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.Notifications;

namespace Core.Services.SubscriptionActions.Actions
{
    public class CancelSubscription : SubscriptionAction
    {

        public CancelSubscription(
            IMapper mapper,
            ICompanySubscriptionServiceFacade companySubscriptionServiceFacade,
            NotificationContext notificationContext,
            IEmailSender emailSender) : base(mapper, companySubscriptionServiceFacade, notificationContext, emailSender)
        {
        }

        public override async Task<object> SpecificAction(CompanySubscription currentSubscription, AccountDTO account, SubscriptionPlan plan)
        {
            if (plan.Price <= 0)
            {
                NotificationContext.AddNotification("Error", "It's not possible to cancel a free subscription.");
                return null;
            }

            currentSubscription.EndSubscription();

            return await CompanySubscriptionServiceFacade.CancelSubscriptionAsync(currentSubscription);
        }

        public override object SendEmailAndReturnValue(CompanySubscription currentSubscription, AccountDTO account, SubscriptionPlan plan)
        {
            SendCancelEmail(account.Email, account.FirstName, plan.Name);

            return true;
        }

        private void SendCancelEmail(string toEmail, string username, string planName)
        {
            EmailSender.SendSubscriptionCancelEmail(toEmail, username, planName);
        }
    }
}
