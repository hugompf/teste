﻿namespace Core.Services.EmailService
{
    public interface IEmailSender
    {
        void SendPaymentConfirmationEmail(string toEmail, string username, string subscriptionPlanName);

        void SendRenewalConfirmationEmail(string toEmail, string username, string subscriptionPlanName, string endDate);

        void SendSubscriptionCancelEmail(string toEmail, string username, string subscriptionPlanName);
    }
}
