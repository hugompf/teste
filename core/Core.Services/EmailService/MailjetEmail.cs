﻿using Core.Services.EmailService.Templates;
using Mailjet.Client;
using Mailjet.Client.Resources;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace Core.Services.EmailService
{
    public class MailjetEmail : IEmailSender
    {
        private readonly IConfiguration _configuration;

        private readonly MailjetClient _mailJetClient;

        public MailjetEmail(IConfiguration config)
        {
            _configuration = config;

            _mailJetClient = new MailjetClient(_configuration["MailJet:PublicKey"], _configuration["MailJet:PrivateKey"])
            {
                Version = ApiVersion.V3_1
            };
        }

        public void SendPaymentConfirmationEmail(string toEmail, string username, string subscriptionPlanName)
        {
            var template = new PaymentConfirmationTemplate(_configuration);

            JObject variables = template.Variables(username, subscriptionPlanName);

            BuildAndSend(template.Subject, toEmail, template.TemplateId, variables);
        }

        public void SendRenewalConfirmationEmail(string toEmail, string username, string subscriptionPlanName, string endDate)
        {
            var template = new RenewalConfirmationTemplate(_configuration);

            JObject variables = template.Variables(username, subscriptionPlanName, endDate);

            BuildAndSend(template.Subject, toEmail, template.TemplateId, variables);
        }

        public void SendSubscriptionCancelEmail(string toEmail, string username, string subscriptionPlanName)
        {
            var template = new SubscriptionCancelTemplate(_configuration);

            JObject variables = template.Variables(username, subscriptionPlanName);

            BuildAndSend(template.Subject, toEmail, template.TemplateId, variables);
        }

        private void BuildAndSend(string subject, string toEmail, int templateId, JObject variables)
        {
            MailjetRequest request = BuildMailJetRequest(subject, toEmail, templateId, variables);

            SendEmail(request);
        }

        private async void SendEmail(MailjetRequest request)
        {
            await _mailJetClient.PostAsync(request);
        }

        private MailjetRequest BuildMailJetRequest(string subject, string toEmail, int templateId, JObject variables)
            => new MailjetRequest
            {
                Resource = Send.Resource
            }
                .Property(Send.Messages, new JArray {
                    new JObject {
                        {"From", new JObject {
                            {"Email", _configuration["MailJet:NoReplyEmail"]},
                            {"Name", _configuration["MailJet:NoReplyName"]}
                        }},
                        {"To", new JArray {
                            new JObject {
                                {"Email", toEmail},
                                {"Name", toEmail}
                            }
                        }},
                        {"TemplateID", templateId},
                        {"TemplateLanguage", true },
                        {"Subject", subject},
                        {"Variables", variables}
                    }
                });
    }
}
