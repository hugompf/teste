﻿using System;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace Core.Services.EmailService.Templates
{
    public class RenewalConfirmationTemplate : EmailTemplate
    {
        public readonly string Subject = "Renovação de subscrição MyWaste";
        private const string Title = "Renovação de subscrição";
        private const string Body = "<h2>Olá {0}.</h2><p>O seu pagamento foi confirmado. A sua subscrição <b>{1}</b> foi renovada, e a sua data de fim foi atualizada para <b>{2}</b>. Obrigado.</p>";

        public RenewalConfirmationTemplate(IConfiguration config) : base(config)
        {
        }

        public JObject Variables(string username, string subscriptionName, string endDate)
        {
            return new JObject
            {
                {"title", Title},
                {"content_body", FormatBody(username, subscriptionName, endDate)}
            };
        }

        private string FormatBody(string username, string subscriptionName, string endDate)
        {
            return String.Format(Body, username, subscriptionName, endDate);
        }
    }
}
