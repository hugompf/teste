﻿using System;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace Core.Services.EmailService.Templates
{
    public class PaymentConfirmationTemplate : EmailTemplate
    {
        public readonly string Subject = "Confirmação de pagamento MyWaste";
        private const string Title = "Confirmação de pagamento";
        private const string Body = "<h2>Olá {0}.</h2><p>O seu pagamento foi confirmado. Pode agora desfrutar da subscrição <b>{1}</b>. Obrigado.</p>";

        public PaymentConfirmationTemplate(IConfiguration config) : base(config)
        {
        }

        public JObject Variables(string username, string subscriptionName)
        {
            return new JObject
            {
                {"title", Title},
                {"content_body", FormatBody(username, subscriptionName)}
            };
        }

        private string FormatBody(string username, string subscriptionName)
        {
            return String.Format(Body, username, subscriptionName);
        }
    }
}
