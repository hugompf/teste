﻿using System;
using Microsoft.Extensions.Configuration;

namespace Core.Services.EmailService.Templates
{
    public class EmailTemplate
    {
        private readonly IConfiguration _configuration;

        public readonly int TemplateId;

        public EmailTemplate(IConfiguration config)
        {
            _configuration = config;

            TemplateId = Int32.Parse(_configuration["Mailjet:TemplateId"]);
        }
    }
}
