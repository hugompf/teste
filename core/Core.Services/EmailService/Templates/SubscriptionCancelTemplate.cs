﻿using System;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace Core.Services.EmailService.Templates
{
    public class SubscriptionCancelTemplate : EmailTemplate
    {
        public readonly string Subject = "Cancelamento de subscrição MyWaste";
        private const string Title = "Cancelamento de subscrição";
        private const string Body = "<h2>Olá {0}.</h2><p>A sua subscrição <b>{1}</b> foi cancelada. Obrigado pelo seu tempo.</p>";

        public SubscriptionCancelTemplate(IConfiguration config) : base(config)
        {
        }

        public JObject Variables(string username, string subscriptionName)
        {
            return new JObject
            {
                {"title", Title},
                {"content_body", FormatBody(username, subscriptionName)}
            };
        }

        private string FormatBody(string username, string subscriptionName)
        {
            return String.Format(Body, username, subscriptionName);
        }
    }
}
