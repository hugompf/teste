﻿using Core.Services.Contracts.Item;
using Core.Services.Contracts.Subscription;
using Core.Services.Contracts.UploadImage;
using Core.Services.EmailService;
using Core.Services.Facades.Contracts;
using Core.Services.Facades.Implementations;
using Core.Services.Implementations.Item;
using Core.Services.Implementations.Subscription;
using Core.Services.Implementations.UploadImages;
using Data.Repositories;
using Data.Repositories.Contracts;
using Data.Repositories.Contracts.Item;
using Data.Repositories.Contracts.Subscription;
using Data.Repositories.implementations.Subscription;
using Data.Repositories.Implementations.Item;
using Data.Repositories.Implementations.Subscription;
using Domain.Gateways;
using File.Repositories;
using File.Repositories.Contracts;
using File.Repositories.UploadActions;
using InspireIT.Notifications;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.loC
{
    public static class BootStrapper
    {
        public static void Register(IServiceCollection services)
        {
            services.AddScoped(typeof(IEmailSender), typeof(MailjetEmail));
            services.AddRepositories();
            services.AddServices();
            services.AddSingleInstances();
            services.AddFacades();
            services.AddTransient();
        }

        private static void AddSingleInstances(this IServiceCollection services)
        {
            services.AddScoped<AuthenticationGateway>();
            services.AddScoped<NotificationContext>();
        }

        private static void AddTransient(this IServiceCollection services)
        {
            services.AddTransient<IImageHandler, ImageHandler>();
            services.AddTransient<IDoFileAction, FileSystem>();
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IAccountRepository), typeof(AccountRepository));
            services.AddScoped(typeof(IRepository<>), typeof(DapperRepository<>));
            services.AddScoped(typeof(ISubscriptionPlanRepository), typeof(SubscriptionPlanRepository));
            services.AddScoped(typeof(ICompanySubscriptionRepository), typeof(CompanySubscriptionRepository));
            services.AddScoped(typeof(ISubscriptionPaymentRepository), typeof(SubscriptionPaymentRepository));
            services.AddScoped(typeof(IInvoiceRepository), typeof(InvoiceRepository));
            services.AddScoped(typeof(IItemRepository), typeof(ItemRepository));
            services.AddScoped(typeof(IFileManager), typeof(FileManager));
        }

        private static void AddServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IAccountService), typeof(AccountService));
            services.AddScoped(typeof(ISubscriptionPlanService), typeof(SubscriptionPlanService));
            services.AddScoped(typeof(ICompanySubscriptionService), typeof(CompanySubscriptionService));
            services.AddScoped(typeof(ISubscriptionPaymentService), typeof(SubscriptionPaymentService));
            services.AddScoped(typeof(IInvoiceService), typeof(InvoiceService));
            services.AddScoped(typeof(IItemService), typeof(ItemService));
        }

        private static void AddFacades(this IServiceCollection services)
        {
            services.AddScoped(typeof(ICompanySubscriptionServiceFacade), typeof(CompanySubscriptionServiceFacade));
        }
    }
}
