﻿using Dapper;
using Data.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class DapperRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IConfiguration _config;

        public DapperRepository(IConfiguration config)
        {
            _config = config;
        }

        internal IDbConnection Connection => new SqlConnection(_config.GetConnectionString("DatabaseConnectionString"));

        public virtual async Task<TEntity> GetAsync(object id)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.GetAsync<TEntity>(id);
                conn.Close();
                return result;
            }
        }

        public virtual async Task<object> AddAsync(TEntity entity)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.InsertAsync(entity);
                conn.Close();
                return result;
            }
        }

        public virtual async Task<object> DeleteAsync(object id)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.DeleteAsync<TEntity>(id);
                conn.Close();
                return result;
            }
        }

        public virtual async Task<object> UpdateAsync(TEntity obj)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.UpdateAsync(obj);
                conn.Close();
                return result;
            }
        }
    }
}
