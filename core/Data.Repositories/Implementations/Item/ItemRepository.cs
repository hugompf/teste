﻿using Dapper;
using Data.Repositories.Contracts.Item;
using Data.Repositories.DTO.Item;
using File.Repositories.UploadActions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using Product = Domain.Entities.Item.Item;

namespace Data.Repositories.Implementations.Item
{
    public class ItemRepository : DapperRepository<Product>, IItemRepository
    {
        private readonly IDoFileAction _doFileAction;

        public ItemRepository(IConfiguration config, IDoFileAction doFileAction) : base(config)
        {
            _doFileAction = doFileAction;
        }

        public async Task<IEnumerable<GetItemDTO>> GetAllItemsAsync()
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryAsync<GetItemDTO>(
                    @"SELECT Title, Price, Item.Description, Quantity, Category.Name AS 'CategoryName', 
                          County.Name AS 'CountyName', Company.Name AS 'CompanyName', MeasurementUnit.Name AS 'MeasurementUnitName', [File].Url AS 'UrlItemImage'
                          FROM Item.Item
                          INNER JOIN [Item].County ON County.Id = Item.CountyId
                          INNER JOIN [Item].Category ON Category.Id = Item.CategoryId
                          INNER JOIN [Item].Company ON Company.Id = Item.CompanyId
                          INNER JOIN [Item].MeasurementUnit ON MeasurementUnit.Id = Item.MeasurementUnitId
                          INNER JOIN [Item].[File] ON [File].ItemId = Item.Id"
                );
                conn.Close();
                return result;
            }
        }

        public async Task<GetItemDTO> GetItemInformation(int itemId)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryFirstOrDefaultAsync<GetItemDTO>(
                    @"SELECT Title, Price, Item.Description, Quantity, Category.Name AS 'CategoryName', 
                          County.Name AS 'CountyName', Company.Name AS 'CompanyName', MeasurementUnit.Name AS 'MeasurementUnitName', [File].Url AS 'UrlItemImage'
                          FROM Item.Item
                          INNER JOIN [Item].County ON County.Id = Item.CountyId
                          INNER JOIN [Item].Category ON Category.Id = Item.CategoryId
                          INNER JOIN [Item].Company ON Company.Id = Item.CompanyId
                          INNER JOIN [Item].MeasurementUnit ON MeasurementUnit.Id = Item.MeasurementUnitId
                          INNER JOIN [Item].[File] ON [File].ItemId = Item.Id
                          WHERE Item.Id = @itemId", new { itemId });
                conn.Close();
                return result;
            }
        }

        public async Task<GetFKItemDTO> GetCategoryCountyCompanyMeasurementUnitId(string categoryName,
            string countyName, string companyName, string measurementUnitName)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryFirstOrDefaultAsync<GetFKItemDTO>(
                    @"SELECT Distinct Category.Id AS 'CategoryId', County.Id AS 'CountyId', Company.Id As 'CompanyId', MeasurementUnit.Id AS 'MeasurementUnitId'
                        FROM Item.Category
                        CROSS JOIN [Item].County
                        CROSS Join [Item].Company
                        CROSS JOIN [Item].MeasurementUnit
                        WHERE Category.Name = @categoryName AND County.Name = @countyName AND Company.Name = @companyName AND
                        MeasurementUnit.Name = @measurementUnitName",
                    new { categoryName, countyName, companyName, measurementUnitName });
                conn.Close();
                return result;
            }
        }

        public async Task<object> AddItemIdToFileAsync(Product item, IFormFile imageFile)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            using (var conn = Connection)
            {
                try
                {
                    conn.Open();
                    var resultItem = await conn.InsertAsync(item);

                    var url = await _doFileAction.UploadImage(imageFile);

                    var newFile = new Domain.Entities.Item.File
                    {
                        Url = url,
                        ItemId = resultItem.Value
                    };

                    var resultFile = await conn.InsertAsync(newFile);

                    conn.Close();
                    scope.Complete();
                    return resultItem;
                }
                catch (Exception e)
                {
                    scope.Dispose();
                    conn.Close();
                    return null;
                }
            }
        }
    }
}

