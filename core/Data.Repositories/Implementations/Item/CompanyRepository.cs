﻿using Data.Repositories.Contracts.Item;
using Domain.Entities.Subscription;
using Microsoft.Extensions.Configuration;

namespace Data.Repositories.Implementations.Item
{
    public class CompanyRepository : DapperRepository<Company>, ICompanyRepository
    {
        public CompanyRepository (IConfiguration config) : base(config)
        {

        }
    }
}
