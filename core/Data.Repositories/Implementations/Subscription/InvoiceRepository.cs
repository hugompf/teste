﻿using Data.Repositories.Contracts.Subscription;
using Domain.Entities.Subscription;
using Microsoft.Extensions.Configuration;

namespace Data.Repositories.implementations.Subscription
{
    public class InvoiceRepository : DapperRepository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(IConfiguration config) : base(config)
        {

        } 
    }
}
