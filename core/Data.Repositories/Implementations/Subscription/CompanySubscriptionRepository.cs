﻿using AutoMapper;
using Dapper;
using Data.Repositories.Contracts.Subscription;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using InspireIT.DapperPagination;
using InspireIT.Extensions;

namespace Data.Repositories.Implementations.Subscription
{
    public class CompanySubscriptionRepository : DapperRepository<CompanySubscription>, ICompanySubscriptionRepository
    {
        private readonly IMapper _mapper;

        public CompanySubscriptionRepository(IConfiguration config, IMapper mapper) : base(config)
        {
            _mapper = mapper;
        }

        public override async Task<object> AddAsync(CompanySubscription companySubscription)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            using (var conn = Connection)
            {
                try
                {
                    // Insert CompanySubscription
                    await conn.InsertAsync(companySubscription);

                    var lastId = await conn.QueryFirstAsync<int?>(
                        @"SELECT TOP 1 Subscription.Id FROM [Subscription].Subscription ORDER BY Subscription.Id DESC");

                    // Sets payment foreign key to CompanySubscription id
                    companySubscription.SubscriptionPayment.SubscriptionId = lastId.Value;

                    // Insert SubscriptionPayment
                    await conn.InsertAsync(companySubscription.SubscriptionPayment);

                    conn.Close();
                    scope.Complete();

                    return lastId;
                }
                catch
                {
                    scope.Dispose();
                    conn.Close();

                    return null;
                }
            }
        }

        public async Task<PagedList<CompanySubscriptionDTO>> GetMySubscriptionsAsync(Guid accountId, PaginationParameters paginationParameters)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var order = paginationParameters.ColumnSortOrder ?? "StartDate DESC";

                var result = await conn.QueryAsync<dynamic>(
                       $@"SELECT id,
                                   StartDate,
                                   EndDate,
                                   Active,
                                   PlanName,
                                   InvoiceId,
                                   COUNT(*) OVER() AS TotalNumber
                            FROM
                              (SELECT Subscription.id,
                                      SubscriptionPayment.Date AS StartDate,
                                      DATEADD(DAY, SubscriptionPlan.Duration, SubscriptionPayment.Date) AS EndDate,
                                      Active,
                                      Name AS PlanName,
                                      InvoiceId,
                                      CompanyId
                               FROM [Subscription].SubscriptionPayment
                               INNER JOIN [Subscription].Subscription ON Subscription.Id = SubscriptionPayment.SubscriptionId
                               INNER JOIN [Subscription].SubscriptionPlan ON SubscriptionPlan.Id = Subscription.SubscriptionPlanId
                               UNION SELECT Subscription.id,
                                            StartDate,
                                            EndDate,
                                            Active,
                                            Name AS PlanName,
                                            NULL AS InvoiceId,
                                            CompanyId
                               FROM [Subscription].Subscription
                               INNER JOIN [Subscription].SubscriptionPlan ON SubscriptionPlan.Id = Subscription.SubscriptionPlanId
                               WHERE Subscription.SubscriptionPlanId = 1) t
                            WHERE t.CompanyId =
                                (SELECT CompanyId
                                 FROM [Subscription].Account
                                 WHERE id = @accountId)  
                            AND t.PlanName LIKE (SELECT CONCAT ('%',@filter,'%'))
                            ORDER BY {order}
                            OFFSET @skip ROWS
                            FETCH FIRST @rowsPerPage ROWS ONLY", new { accountId, filter = paginationParameters.Filter, skip = paginationParameters.Skip,
                                                                            rowsPerPage = paginationParameters.RowsPerPage });

                var items = _mapper.Map<IEnumerable<CompanySubscriptionDTO>>(result);
                var totalNumber = Convert.ToInt32(result.ToList().GetPropertyOfAnElementOnAList("TotalNumber"));
                conn.Close();

                return new PagedList<CompanySubscriptionDTO>(items, paginationParameters.PageNumber, totalNumber, paginationParameters.RowsPerPage);
            }
        }

        public async Task<CompanySubscriptionDTO> GetNonFreeActiveSubscriptionByMicrosite(int companyId, int micrositeId)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryFirstOrDefaultAsync<CompanySubscriptionDTO>(
                    @"SELECT SubscriptionPlan.id,
                                StartDate,
                                EndDate,
                                Active,
                                MicrositeId
                         FROM [Subscription].Subscription
                         INNER JOIN [Subscription].SubscriptionPlan ON SubscriptionPlan.id = Subscription.SubscriptionPlanId 
                         AND SubscriptionPlan.Price != 0
                         WHERE Active = 1
                         AND CompanyId = @companyId
                         AND MicrositeId = @micrositeId", new { companyId, micrositeId });
                conn.Close();

                return result;
            }
        }

        public async Task<CompanySubscription> GetCompanyActiveFreeSubscriptionByMicrosite(int companyId, int micrositeId)
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryFirstOrDefaultAsync<CompanySubscription>(
                    @"	  SELECT Subscription.Id,
                                StartDate,
                                EndDate,
                                Active,
                                CompanyId,
                                SubscriptionPlanId,
                                MicrositeId
                         FROM [Subscription].Subscription
                         INNER JOIN [Subscription].SubscriptionPlan ON SubscriptionPlan.id = Subscription.SubscriptionPlanId 
                         AND SubscriptionPlan.Price = 0
                         WHERE Active = 1
                         AND CompanyId = @companyId
                         AND MicrositeId = @micrositeId", new { companyId, micrositeId });
                conn.Close();

                return result;
            }
        }

        public async Task<object> RenewSubscriptionAsync(CompanySubscription companySubscription)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            using (var conn = Connection)
            {
                try
                {
                    // Update CompanySubscription
                    await conn.UpdateAsync(companySubscription);

                    // Sets payment foreign key to CompanySubscription id
                    companySubscription.SubscriptionPayment.SubscriptionId = companySubscription.Id;

                    // Insert SubscriptionPayment
                    await conn.InsertAsync(companySubscription.SubscriptionPayment);

                    conn.Close();
                    scope.Complete();

                    return companySubscription.Id;
                }
                catch
                {
                    scope.Dispose();
                    conn.Close();

                    return null;
                }
            }
        }

        public async Task<object> CancelSubscriptionAsync(CompanySubscription companySubscription)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            using (var conn = Connection)
            {
                try
                {
                    // Update CompanySubscription
                    var res = await conn.UpdateAsync(companySubscription);

                    // Creates new free subscription
                    var freeSubscriptionInfo = await conn.QueryFirstAsync<SubscriptionDurationDTO>(
                        @"SELECT SubscriptionPlan.Id, SubscriptionPlan.Duration FROM Subscription.SubscriptionPlan WHERE Price = 0");

                    var freeSubscription = new CompanySubscription();

                    freeSubscription.StartSubscription(freeSubscriptionInfo.Duration, companySubscription.CompanyId, freeSubscriptionInfo.Id, companySubscription.MicrositeId);

                    // Insert freeSubscription
                    await conn.InsertAsync(freeSubscription);

                    conn.Close();
                    scope.Complete();

                    return res;
                }
                catch (Exception e)
                {
                    scope.Dispose();
                    conn.Close();

                    return null;
                }
            }
        }
    }
}