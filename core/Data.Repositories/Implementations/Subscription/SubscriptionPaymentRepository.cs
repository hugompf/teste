﻿using Data.Repositories.Contracts.Subscription;
using Domain.Entities.Subscription;
using Microsoft.Extensions.Configuration;

namespace Data.Repositories.Implementations.Subscription
{
    public class SubscriptionPaymentRepository : DapperRepository<SubscriptionPayment>, ISubscriptionPaymentRepository
    {
        public SubscriptionPaymentRepository(IConfiguration config) : base(config)
        {

        }
    }
}
