﻿﻿using System;
using AutoMapper;
using Data.Repositories.Contracts.Subscription;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using Domain.Gateways;
using Microsoft.Extensions.Configuration;

namespace Data.Repositories.Implementations.Subscription
{
    public class AccountRepository : DapperRepository<Account>, IAccountRepository
    {
        private readonly AuthenticationGateway _authGateway;
        private readonly IMapper _mapper;

        public AccountRepository(
            IConfiguration config,
            AuthenticationGateway authGateway,
            IMapper mapper) : base(config)
        {
            _authGateway = authGateway;
            _mapper = mapper;
        }

        public AccountDTO GetAccountWithEmail(Guid id)
        {
            var result = new AccountDTO();
            result.Id = id;

            var userDb = GetAsync(id).Result;
            result.FirstName = userDb.FirstName;
            result.LastName = userDb.LastName;
            result.CompanyId = userDb.CompanyId;

            var userAuth = _authGateway.GetUserAuthentication(id);
            result.Email = userAuth.Email.EmailAddress;

            return result;
        }
    }
}
