﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Data.Repositories.Contracts.Subscription;
using Domain.Entities.Subscription;
using Microsoft.Extensions.Configuration;

namespace Data.Repositories.Implementations.Subscription
{
    public class SubscriptionPlanRepository : DapperRepository<SubscriptionPlan>, ISubscriptionPlanRepository
    {
        public SubscriptionPlanRepository(IConfiguration config) : base(config)
        {

        }

        public async Task<IEnumerable<SubscriptionPlan>> GetAllSubscriptionPlansAsync()
        {
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.GetListAsync<SubscriptionPlan>();
                conn.Close();
                return result;
            }
        }
    }
}
