﻿using System.Collections.Generic;

namespace Data.Repositories.Util
{
    public class Rollbacker
    {
        private readonly List<KeyValuePair<object, object>> addedValues;

        public Rollbacker()
        {
            addedValues = new List<KeyValuePair<object, object>>();
        }

        public void AddInserted(object rep, object id)
        {
            addedValues.Add(new KeyValuePair<object, object>(rep, id));
        }

        public void Rollback()
        {
            addedValues.Reverse();

            foreach (var rep in addedValues)
            {
                var repository = rep.Key;
                var param = new[] { rep.Value };

                repository.GetType().GetMethod("DeleteAsync").Invoke(repository, param);
            }

            ResetAddedValues();
        }

        public void ResetAddedValues()
        {
            //At the end of the transaction, empties the addedValues list
            addedValues.Clear();
        }
    }
}
