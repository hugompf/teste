﻿using System;
using System.Threading.Tasks;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.DapperPagination;

namespace Data.Repositories.Contracts.Subscription
{
    public interface ICompanySubscriptionRepository : IRepository<CompanySubscription> 
    {
        Task<PagedList<CompanySubscriptionDTO>> GetMySubscriptionsAsync(Guid accountId, PaginationParameters paginationParameters);
        Task<CompanySubscriptionDTO> GetNonFreeActiveSubscriptionByMicrosite(int companyId, int micrositeId);
        Task<CompanySubscription> GetCompanyActiveFreeSubscriptionByMicrosite(int companyId, int micrositeId);
        Task<object> RenewSubscriptionAsync(CompanySubscription companySubscription);
        Task<object> CancelSubscriptionAsync(CompanySubscription companySubscription);
    }
}
