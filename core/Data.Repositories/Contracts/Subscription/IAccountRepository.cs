﻿using System;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;

namespace Data.Repositories.Contracts.Subscription
{
    public interface IAccountRepository : IRepository<Account>
    {
        AccountDTO GetAccountWithEmail(Guid id);
    }
}
