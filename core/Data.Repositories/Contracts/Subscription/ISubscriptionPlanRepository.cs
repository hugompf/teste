﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities.Subscription;

namespace Data.Repositories.Contracts.Subscription
{
    public interface ISubscriptionPlanRepository : IRepository<SubscriptionPlan>
    {
        Task<IEnumerable<SubscriptionPlan>> GetAllSubscriptionPlansAsync();
    }
}

