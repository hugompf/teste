﻿using Domain.Entities.Subscription;

namespace Data.Repositories.Contracts.Subscription
{
    public interface ISubscriptionPaymentRepository : IRepository<SubscriptionPayment>
    {
    }
}

