﻿using Data.Repositories.DTO.Item;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Product = Domain.Entities.Item.Item;

namespace Data.Repositories.Contracts.Item
{
    public interface IItemRepository : IRepository<Product>
    {
       Task<IEnumerable<GetItemDTO>> GetAllItemsAsync();

       Task<GetItemDTO> GetItemInformation(int itemId);

       Task<GetFKItemDTO> GetCategoryCountyCompanyMeasurementUnitId(string categoryName, string countyName, string companyName, string measurementUnitName);

       Task<object> AddItemIdToFileAsync(Product item, IFormFile imageFile);
    }
}
