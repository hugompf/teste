﻿using Domain.Entities.Subscription;

namespace Data.Repositories.Contracts.Item
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }
}

