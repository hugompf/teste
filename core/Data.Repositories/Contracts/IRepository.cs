﻿using System.Threading.Tasks;

namespace Data.Repositories.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(object id);

        Task<object> AddAsync(TEntity entity);

        Task<object> DeleteAsync(object id);

        Task<object> UpdateAsync(TEntity obj);
    }
}
