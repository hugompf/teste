﻿namespace Data.Repositories.DTO.Item
{
    public class GetItemDTO
    {
        public string Title { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public string CategoryName { get; set; }

        public string CountyName { get; set; }

        public string CompanyName { get; set; }

        public string MeasurementUnitName { get; set; }

        public string UrlItemImage { get; set; }
    }
}