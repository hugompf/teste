﻿namespace Data.Repositories.DTO.Item
{
    public class GetFKItemDTO
    {
        public int CategoryId { get; set; }

        public int CountyId { get; set; }

        public int CompanyId { get; set; }

        public int MeasurementUnitId { get; set; }
    }
}
