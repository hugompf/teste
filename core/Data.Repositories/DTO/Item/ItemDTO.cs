﻿using Microsoft.AspNetCore.Http;

namespace Data.Repositories.DTO.Item
{
    public class ItemDTO
    {
       public string Title { get; set; }

       public decimal Price { get; set; }

       public string Description { get; set; }

       public int Quantity { get; set; }

       public CategoryDTO Category { get; set; }

       public CountyDTO County { get; set; }

       public CompanyDTO Company { get; set; }

       public MeasurementUnitDTO MeasurementUnit { get; set; }

       public class CategoryDTO
        {
            public string Name { get; set; }
        }
        public class CountyDTO
        {
            public string Name { get; set; }
        }

        public class CompanyDTO
        {
            public string Name { get; set; }
        }

        public class MeasurementUnitDTO
        {
            public string Name { get; set; }
        }
        
        public string UrlItemImage { get; set; }

        public IFormFile ItemImage { get; set; }
    }
}