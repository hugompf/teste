﻿using System;

namespace Data.Repositories.DTO.SubscriptionDTO
{
    public class CompanySubscriptionDTO
    {
        public int Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool Active { get; set; }

        public string PlanName { get; set; }

        public int InvoiceId { get; set; }

        public int MicrositeId { get; set; }
    }
}
