﻿using System;

namespace Data.Repositories.DTO.SubscriptionDTO
{
    public class SubscriptionDurationDTO
    {
        public int Id { get; set; }

        public int Duration { get; set; }
    }
}
