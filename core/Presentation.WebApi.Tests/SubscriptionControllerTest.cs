using System;
using System.Linq;
using AutoFixture;
using Moq;
using Presentation.WebApi.Controllers.Subscription;
using Xunit;
using AutoMapper;
using Core.Services.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Core.Services.EmailService;
using Core.Services.Facades.Implementations;
using Core.Services.Implementations.Subscription;
using Data.Repositories.Contracts.Subscription;
using Data.Repositories.DTO.SubscriptionDTO;
using FluentAssertions;
using InspireIT.DapperPagination;
using InspireIT.Notifications;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.WebApi.Tests
{
    public class SubscriptionControllerTest
    {
        private readonly Fixture _fixture;
        private readonly SubscriptionController _subscriptionController;
        private readonly Mock<ICompanySubscriptionRepository> _companySubscriptionRepository;
        private const int DataSize = 10;

        public SubscriptionControllerTest()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });
            _fixture = new Fixture();
            var configMapper = config.CreateMapper();

            _companySubscriptionRepository = new Mock<ICompanySubscriptionRepository>();
            _subscriptionController = new SubscriptionController(new CompanySubscriptionService(config.CreateMapper(), new CompanySubscriptionServiceFacade(_companySubscriptionRepository.Object,
                    new Mock<IAccountRepository>().Object, new Mock<ISubscriptionPlanRepository>().Object),
                new Mock<NotificationContext>().Object, new Mock<IEmailSender>().Object), configMapper, new Mock<ISubscriptionPlanService>().Object);
        }

        [Fact]
        public async void GetMySubscriptions_Success()
        {
            SetupGetMySubscriptions(DataSize);
            var result = await _subscriptionController.GetMySubscriptions(1, DataSize, null, null);
            result.Should().NotBeNull(because: "the request was made correctly");
            result.Should().BeOfType<OkObjectResult>();
            var value = result.As<OkObjectResult>();
            value.Value.Should().BeOfType<CompanySubscriptionPaginationJson>("it's the expected return type");
            value.Value.As<CompanySubscriptionPaginationJson>().Items.Count().Should().Be(DataSize, "it's the expected list size");
        }

        #region === Private Methods ===
        private void SetupGetMySubscriptions(int dataSize)
        {
            var items = _fixture.Build<CompanySubscriptionDTO>().CreateMany(dataSize);

            var dataSet = _fixture.Build<PagedList<CompanySubscriptionDTO>>()
                .With(p => p.Items, items).Create();

            _companySubscriptionRepository.Setup(r => r.GetMySubscriptionsAsync(It.IsAny<Guid>(), It.IsAny<PaginationParameters>())).ReturnsAsync(dataSet);
        }

        private void SetupInternalError()
        {
            _companySubscriptionRepository.Setup(r => r.GetMySubscriptionsAsync(It.IsAny<Guid>(), It.IsAny<PaginationParameters>())).Throws(new Exception());
        }
        #endregion
    }
}
