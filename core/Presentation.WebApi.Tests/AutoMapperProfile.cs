﻿using AutoMapper;
using Core.Services.DTO.Subscription;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;
using InspireIT.DapperPagination;
using Presentation.WebApi.Models.Subscription;

namespace Presentation.WebApi.Tests
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            MapSingle();
        }

        private void MapSingle()
        {
            CreateMap<CompanySubscription, CompanySubscriptionDTO>().ReverseMap();
            CreateMap<CompanySubscriptionDTO, MyCompanySubscriptionsDTO>();
            CreateMap<PagedList<CompanySubscriptionDTO>, PagedList<MyCompanySubscriptionsDTO>>();
            CreateMap<MyCompanySubscriptionsDTO, CompanySubscriptionsJson>().ReverseMap();
            CreateMap<CompanySubscriptionPaginationJson, PagedList<MyCompanySubscriptionsDTO>>();
        }
    }
}
