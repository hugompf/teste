using AutoFixture;
using Moq;
using Presentation.WebApi.Controllers.Subscription;
using Xunit;
using AutoMapper;
using Core.Services.Implementations.Subscription;
using Data.Repositories.Contracts.Subscription;
using Domain.Entities;
using Domain.Entities.Subscription;
using File.Repositories.Contracts;
using File.Repositories.DTO;
using FluentAssertions;
using InspireIT.Notifications;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.WebApi.Tests
{
    public class InvoiceControllerTest
    {
        private readonly Fixture _fixture;
        private readonly InvoiceController _invoiceController;
        private readonly Mock<IInvoiceRepository> _invoiceRepository;
        private readonly Mock<IFileManager> _fileManager;


        public InvoiceControllerTest()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });
            _fixture = new Fixture();

            _invoiceRepository = new Mock<IInvoiceRepository>();
            _fileManager = new Mock<IFileManager>();
            _invoiceController = new InvoiceController(new InvoiceService(config.CreateMapper(),_invoiceRepository.Object,_fileManager.Object, new Mock<NotificationContext>().Object),
                config.CreateMapper(), new Mock<NotificationContext>().Object);
        }

        [Fact]
        public async void GetInvoice_Success()
        {
            SetupGetInvoice();
            var result = await _invoiceController.GetInvoice(1);
            result.Should().NotBeNull(because: "the request was made correctly");
            result.Should().BeOfType<FileContentResult>();
            var value = result.As<FileContentResult>();
            value.Should().BeOfType<FileContentResult>("it's the expected return type");
            value.FileContents.Should().NotBeNull("it's expected return data");
        }

        #region === Private Methods ===
        private void SetupGetInvoice()
        {
            var invoice = _fixture.Build<Invoice>().Create();
            var data = _fixture.Build<FileDTO>().Create();

            _invoiceRepository.Setup(r => r.GetAsync(It.IsAny<int>())).ReturnsAsync(invoice);
            _fileManager.Setup(f => f.Load(It.IsAny<LoadFileModel>())).Returns(data);
        }

        private void SetupGetInvoiceRepositoryNullValue()
        {
            _invoiceRepository.Setup(r => r.GetAsync(It.IsAny<int>())).ReturnsAsync((Invoice)null);
        }

        private void SetupGetInvoiceFileManagerNullValue()
        {
            _fileManager.Setup(r => r.Load(It.IsAny<LoadFileModel>())).Returns((FileDTO)null);
        }
        #endregion
    }
}
