﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Item;

namespace Domain.Entities.Subscription
{
    [Table("Subscription", Schema = "Subscription")]
    public class CompanySubscription
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [Required]
        public int SubscriptionPlanId { get; set; }

        [Required]
        public int MicrositeId { get; set; }

        public virtual SubscriptionPlan Plan { get; set; }

        public virtual Company Company { get; set; }

        public virtual SubscriptionPayment SubscriptionPayment { get; set; }

        public virtual Microsite Microsite { get; set; }
        
        public void StartSubscription(int duration, int companyId, int planId, int micrositeId)
        {
            StartDate = DateTime.Now;
            EndDate = CalculateEndDate(duration);

            Active = true;

            CompanyId = companyId;
            SubscriptionPlanId = planId;
            MicrositeId = micrositeId;
        }

        private DateTime AddDurationToDate(DateTime date, int duration)
        {
            // AddDays adds the duration - 1. 
            // Ex: 2019-03-25 + 365 = 2020-03-24
            int addDaysAdjustment = 1;

            try
            {
                return date.AddDays(duration + addDaysAdjustment);
            }
            catch (ArgumentOutOfRangeException e)
            {
                return DateTime.MaxValue;
            }
        }

        private DateTime CalculateEndDate(int duration)
        {
            return AddDurationToDate(StartDate, duration);
        }

        public int DurationInDays()
        {
            return (EndDate - StartDate).Days;
        }

        public void EndSubscription()
        {
            EndDate = DateTime.Now;

            Active = false;
        }

        public void Renew(int duration)
        {
            var newEndDate = AddDurationToDate(EndDate, duration);

            EndDate = newEndDate;
        }
    }
}
