﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Auth;

namespace Domain.Entities.Subscription
{
    [Table("Account", Schema = "Subscription")]
    public class Account
    {
        [Column("Id")]
        [Key]
        public Guid Id { get; set; }

        [Column("FirstName")]
        [Required]
        [MaxLength(70)]
        public string FirstName { get; set; }

        [Column("LastName")]
        [Required]
        [MaxLength(70)]
        public string LastName { get; set; }

        [Column("CompanyId")]
        [Required]
        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public AuthAccount AuthAccount { get; set; }
        
        [Editable(false)]
        public string DisplayName => $"{FirstName} {LastName}";

    }
}
