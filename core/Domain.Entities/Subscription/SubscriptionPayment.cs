﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Subscription
{
    [Table("SubscriptionPayment", Schema = "Subscription")]
    public class SubscriptionPayment
    {
        [Column("Id")]
        [Key]
        public int Id { get; set; }

        [Column("Date")]
        [Required]
        public DateTime Date { get; set; }

        [Column("AccountId")]
        [Required]
        public Guid AccountId { get; set; }

        [Column("SubscriptionId")]
        [Required]
        public int SubscriptionId { get; set; }

        [Column("InvoiceId")]
        public int? InvoiceId { get; set; }

        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        [ForeignKey("SubscriptionId")]
        public virtual CompanySubscription Subscription { get; set; }

        public void SetCurrentDate()
        {
            Date = DateTime.Now;
        }
    }
}
