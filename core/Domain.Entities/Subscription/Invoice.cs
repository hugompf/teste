﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Subscription
{
    [Table("Invoice", Schema = "Subscription")]
    public class Invoice
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string FilePath { get; set; }

        [Required]
        public DateTime RegistrationDate { get; set; }
    }
}
