﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Subscription
{
    [Table("SubscriptionPlan", Schema = "Subscription")]
    public class SubscriptionPlan
    {
        [Column("Id")]
        [Key]
        public int Id { get; set; }

        [Column("Name")]
        [Required]
        [MaxLength(70)]
        public string Name { get; set; }

        [Column("Duration")]
        [Required]
        public int Duration { get; set; }

        [Column("Price")]
        [Required]
        public double Price { get; set; }

        [Column("MonthlyContactRequestLimit")]
        [Required]
        public int MonthlyContactRequestLimit { get; set; }

        [Column("MonthlyItemHighlightLimit")]
        [Required]
        public int MonthlyItemHighlightLimit { get; set; }

        [Column("NotificationsAvailable")]
        [Required]
        public bool NotificationsAvailable { get; set; }
    }
}
