﻿namespace Domain.Entities
{
    public class LoadFileModel
    {
        public FileTypes Type { get; set; }
        public string Path { get; set; }
    }
}
