﻿namespace Domain.Entities
{
    public enum FileTypes
    {
        Invoice,
        ItemImage,
        NewImage
    };

    public class UploadFileModel
    {
        public FileTypes Type { get; set; }
        public string File { get; set; }
    }
}
