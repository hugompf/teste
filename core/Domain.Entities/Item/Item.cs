﻿using Domain.Entities.Subscription;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Item
{
    [Table("Item", Schema = "Item")]
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        public DateTime RegistrationDate { get; set; }

        [Required]
        [MaxLength(100)]
        public string Description { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        public bool Highligthed { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int CountyId { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [Required]
        public int MeasurementUnitId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [ForeignKey("CountyId")]
        public virtual County County { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [ForeignKey("MeasurementUnitId")]
        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public IEnumerable<File> Files { get; set; }

        public void CreateNewItem()
        {
            RegistrationDate = DateTime.Now;
            Active = true;
            Highligthed = false;
        }
    }
}
