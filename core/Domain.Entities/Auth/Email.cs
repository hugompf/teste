﻿namespace Domain.Entities.Auth
{
    public class Email
    {
        public string EmailAddress { get; set; }
    }
}