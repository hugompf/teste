using System;
using System.Linq;
using AutoFixture;
using AutoMapper;
using Core.Services.EmailService;
using Core.Services.Implementations.Subscription;
using Data.Repositories.DTO.SubscriptionDTO;
using Core.Services.DTO.Subscription;
using Core.Services.Facades.Implementations;
using Data.Repositories.Contracts.Subscription;
using FluentAssertions;
using InspireIT.DapperPagination;
using InspireIT.Notifications;
using Moq;
using Xunit;

namespace Core.Services.Tests
{
    public class CompanySubscriptionServiceTests
    {
        private readonly Fixture _fixture;
        private readonly CompanySubscriptionService _companySubscriptionService;
        private readonly Mock<ICompanySubscriptionRepository> _companySubscriptionRepository;
        private readonly PaginationParameters _parameters; 

        public CompanySubscriptionServiceTests()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfile>();
            });

            _fixture = new Fixture();
            _companySubscriptionRepository = new Mock<ICompanySubscriptionRepository>();
            _companySubscriptionService = new CompanySubscriptionService(config.CreateMapper(), new CompanySubscriptionServiceFacade(_companySubscriptionRepository.Object,
                    new Mock<IAccountRepository>().Object, new Mock<ISubscriptionPlanRepository>().Object), 
                    new Mock<NotificationContext>().Object, new Mock<IEmailSender>().Object);
            _parameters = new PaginationParameters(1, 10, null, null);
        }
        
        [Fact]
        public async void GetMySubscriptionsAsync_SuccessAsync()
        {
            var accountId = SetupGetMySubscriptions(10);

            var result = await _companySubscriptionService.GetMySubscriptionsAsync(accountId, _parameters);
            result.Should().NotBeNull(because: "a data set was setup");
            result.Should().BeOfType<PagedList<MyCompanySubscriptionsDTO>>(because: "it's the expected return type");
            result.Items.ToList().Count.Should().Be(10, "it's the expected return list size");
        }

        [Fact]
        public async void GetMySubscriptionsAsync_Success_EmptyList()
        {
            var accountId = SetupGetMySubscriptions(0);

            var result = await _companySubscriptionService.GetMySubscriptionsAsync(accountId, _parameters);
            result.Should().NotBeNull(because: "a data set was setup");
            result.Should().BeOfType<PagedList<MyCompanySubscriptionsDTO>>(because: "it's the expected return type");
            result.Items.ToList().Count.Should().Be(0, "it's the expected return list size");
        }

        #region === Private Methods ===
        private Guid SetupGetMySubscriptions(int dataSize)
        {
            var accountId = new Guid();

            var items = _fixture.Build<CompanySubscriptionDTO>().CreateMany(dataSize);

            var dataSet = _fixture.Build<PagedList<CompanySubscriptionDTO>>()
                .With(p => p.Items , items).Create();

            _companySubscriptionRepository.Setup(r => r.GetMySubscriptionsAsync(accountId, _parameters)).ReturnsAsync(dataSet);
            return accountId;
        }
        #endregion

    }
}
