﻿using AutoMapper;
using Core.Services.DTO.Subscription;
using InspireIT.DapperPagination;
using Data.Repositories.DTO.SubscriptionDTO;
using Domain.Entities.Subscription;

namespace Core.Services.Tests
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CompanySubscription, CompanySubscriptionDTO>().ReverseMap();         
            CreateMap<CompanySubscriptionDTO, MyCompanySubscriptionsDTO>();      
            CreateMap<PagedList<CompanySubscriptionDTO>, PagedList<MyCompanySubscriptionsDTO>>();
        }
    }
}
