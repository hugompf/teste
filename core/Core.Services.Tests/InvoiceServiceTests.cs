using AutoFixture;
using AutoMapper;
using Core.Services.Implementations.Subscription;
using Data.Repositories.Contracts.Subscription;
using Core.Services.DTO.Subscription;
using Domain.Entities;
using Domain.Entities.Subscription;
using File.Repositories.Contracts;
using File.Repositories.DTO;
using FluentAssertions;
using InspireIT.Notifications;
using Moq;
using Xunit;

namespace Core.Services.Tests
{
    public class InvoiceServiceTests
    {
        private readonly Mock<IMapper> _mapper;
        private readonly Fixture _fixture;
        private readonly InvoiceService _invoiceService;
        private readonly Mock<IInvoiceRepository> _invoiceRepository;
        private readonly Mock<IFileManager> _fileManager;

        public InvoiceServiceTests()
        {
            _fixture = new Fixture();
            _mapper = new Mock<IMapper>();
            _invoiceRepository = new Mock<IInvoiceRepository>();
            _fileManager = new Mock<IFileManager>();
            _invoiceService = new InvoiceService(_mapper.Object, _invoiceRepository.Object, _fileManager.Object, new Mock<NotificationContext>().Object);
        }

        [Fact]
        public void GetAsync_Success()
        {
            var invoiceId = SetupGetFile();

            var objectMapped = _fixture.Build<InvoiceFileDTO>().Create();

            _mapper.Setup(m =>
                    m.Map<InvoiceFileDTO>(It.IsAny<InvoiceFileDTO>()))
                .Returns(objectMapped);

            var result = _invoiceService.GetAsync(invoiceId).Result;
            result.Should().NotBeNull(because: "a data set was setup");
            result.Should().BeOfType<InvoiceFileDTO>(because: "it's the expected return type");
        }

        [Fact]
        public void GetAsync_File_Doesnt_Exist()
        {
            var invoiceId = 1;

            _invoiceRepository.Setup(r => r.GetAsync(invoiceId)).ReturnsAsync((Invoice)null);

            var result = _invoiceService.GetAsync(invoiceId).Result;
            result.Should().BeNull(because: "The file doesn't exists");
        }

        [Fact]
        public void GetAsync_File_Path_Doesnt_Exist()
        {
            var invoiceId = SetupGetFile();

            SetupFileManager();

            var dataSet = _fixture.Build<Invoice>().
                With(p => p.Id, invoiceId).
                Create();
            
            _invoiceRepository.Setup(r => r.GetAsync(invoiceId)).ReturnsAsync(dataSet);
           
            var result = _invoiceService.GetAsync(invoiceId).Result;
            result.Should().BeNull(because: "a data set was setup");
        }

        #region === Private Methods ===
        private int SetupGetFile()
        {
            var invoiceId = 1;

            var dataSet = _fixture.Build<Invoice>().With(p => p.Id, invoiceId)
                .Create();

            _invoiceRepository.Setup(r => r.GetAsync(invoiceId)).ReturnsAsync(dataSet);

            return invoiceId;
        }

        private void SetupFileManager()
        {
            var fileModel = _fixture.Build<LoadFileModel>().
                With(p => p.Path, "").
                Create();
            _fileManager.Setup(fm => fm.Load(fileModel)).Returns((FileDTO)null);

        }
        #endregion
    }
}
