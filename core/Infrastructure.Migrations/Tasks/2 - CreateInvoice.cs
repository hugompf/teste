﻿using System;
using FluentMigrator;

namespace Infrastructure.Migrations.Tasks
{
    [Migration(2)]
    public class CreateInvoice : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema("Subscription").Table("Invoice").Exists())
            {
                Create.Table("Invoice").InSchema("Subscription")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("FilePath").AsString(100).NotNullable()
                    .WithColumn("RegistrationDate").AsString(100).NotNullable();
            }
            Populate();
        }

        public override void Down()
        {
            if (Schema.Schema("Subscription").Table("Invoice").Exists())
            {
                Delete.Table("Invoice").InSchema("Subscription");
            }
        }

        private void Populate()
        {
            Insert.IntoTable("Invoice").InSchema("Subscription").Row(
                new
                {
                    FilePath = "\\fatura1.pdf",
                    RegistrationDate = new DateTime(2017, 03, 29)
                });
            Insert.IntoTable("Invoice").InSchema("Subscription").Row(
                new
                {
                    FilePath = "\\fatura2.pdf",
                    RegistrationDate = new DateTime(2018, 03, 29)
                });
        }

    }
}
