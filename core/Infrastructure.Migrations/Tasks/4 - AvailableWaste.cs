﻿using System;
using FluentMigrator;

namespace Infrastructure.Migrations.Tasks
{
    [Migration(4)]
    public class AvailableWaste : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema("Item").Table("MeasurementUnit").Exists())
            {
                Create.Table("MeasurementUnit").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Name").AsString(30).NotNullable()
                    .WithColumn("Shortened").AsString(5).NotNullable();
            }

            if (!Schema.Schema("Item").Table("Category").Exists())
            {
                Create.Table("Category").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Name").AsString(70).NotNullable()
                    .WithColumn("ParentCategoryId").AsInt32().Nullable();

            }

            if (!Schema.Schema("Item").Table("Microsite").Exists())
            {
                Create.Table("Microsite").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Active").AsBoolean().NotNullable()
                    .WithColumn("Url").AsString(100).NotNullable()
                    .WithColumn("CategoryId").AsInt32();
            }

            if (!Schema.Schema("Item").Table("Item").Exists())
            {
                Create.Table("Item").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Title").AsString(50).NotNullable()
                    .WithColumn("Price").AsDecimal().NotNullable()
                    .WithColumn("RegistrationDate").AsDateTime().NotNullable()
                    .WithColumn("Description").AsString(100).NotNullable()
                    .WithColumn("Active").AsBoolean().NotNullable()
                    .WithColumn("Highligthed").AsBoolean().NotNullable()
                    .WithColumn("Quantity").AsInt32().NotNullable()
                    .WithColumn("CategoryId").AsInt32().NotNullable()
                    .WithColumn("CountyId").AsInt32().NotNullable()
                    .WithColumn("CompanyId").AsInt32().NotNullable()
                    .WithColumn("MeasurementUnitId").AsInt32().NotNullable();
            }

            if (!Schema.Schema("Item").Table("File").Exists())
            {
                Create.Table("File").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Url").AsString(100).NotNullable()
                    .WithColumn("ItemId").AsInt32().NotNullable();
            }

            CreateForeignKeys();
            Inserts();
        }

        public override void Down()
        {
            if (Schema.Schema("Item").Table("File").Exists())
            {
                Delete.Table("File").InSchema("Item");
            }

            if (Schema.Schema("Item").Table("Item").Exists())
            {
                Delete.Table("Item").InSchema("Item");
            }

            if (Schema.Schema("Item").Table("Microsite").Exists())
            {
                Delete.Table("Microsite").InSchema("Item");
            }

            if (Schema.Schema("Item").Table("Category").Exists())
            {
                Delete.Table("Category").InSchema("Item");
            }

            if (Schema.Schema("Item").Table("MeasurementUnit").Exists())
            {
                Delete.Table("MeasurementUnit").InSchema("Item");
            }
        }
        
        private void CreateForeignKeys()
        {
            if (!Schema.Schema("Item").Table("Category").Constraint("FK_Category_SubCategory").Exists())
            {
                Create.ForeignKey("FK_Category_SubCategory")
                    .FromTable("Category").InSchema("Item").ForeignColumn("ParentCategoryId")
                    .ToTable("Category").InSchema("Item").PrimaryColumn("Id");
            }
            
            if (!Schema.Schema("Item").Table("Microsite").Constraint("FK_Microsite_Category").Exists())
            {
                Create.ForeignKey("FK_Microsite_Category")
                    .FromTable("Microsite").InSchema("Item").ForeignColumn("CategoryId")
                    .ToTable("Category").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Item").Table("Item").Constraint("FK_Item_Category").Exists())
            {
                Create.ForeignKey("FK_Item_Category")
                    .FromTable("Item").InSchema("Item").ForeignColumn("CategoryId")
                    .ToTable("Category").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Item").Table("Item").Constraint("FK_Item_County").Exists())
            {
                Create.ForeignKey("FK_Item_County")
                    .FromTable("Item").InSchema("Item").ForeignColumn("CountyId")
                    .ToTable("County").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Item").Table("Item").Constraint("FK_Item_MeasurementUnit").Exists())
            {
                Create.ForeignKey("FK_Item_MeasurementUnit")
                    .FromTable("Item").InSchema("Item").ForeignColumn("MeasurementUnitId")
                    .ToTable("MeasurementUnit").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Item").Table("Item").Constraint("FK_Item_Company").Exists())
            {
                Create.ForeignKey("FK_Item_Company")
                    .FromTable("Item").InSchema("Item").ForeignColumn("CompanyId")
                    .ToTable("Company").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Item").Table("File").Constraint("FK_File_Item").Exists())
            {
                Create.ForeignKey("FK_File_Item")
                    .FromTable("File").InSchema("Item").ForeignColumn("ItemId")
                    .ToTable("Item").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Subscription").Table("SubscriptionPayment").Constraint("FK_SubscriptionPayment_Subscription").Exists())
            {
                Create.ForeignKey("FK_SubscriptionPayment_Subscription")
                    .FromTable("SubscriptionPayment").InSchema("Subscription").ForeignColumn("SubscriptionId")
                    .ToTable("Subscription").InSchema("Subscription").PrimaryColumn("Id");
            }

            // Subscription Microsite foreign key
            if (!Schema.Schema("Subscription").Table("Subscription").Constraint("FK_Subscription_Microsite").Exists())
            {
                Create.ForeignKey("FK_Subscription_Microsite")
                    .FromTable("Subscription").InSchema("Subscription").ForeignColumn("MicrositeId")
                    .ToTable("Microsite").InSchema("Item").PrimaryColumn("Id");
            }
        }

        private void Inserts()
        {
            Insert.IntoTable("MeasurementUnit").InSchema("Item").Row(new
            {
                Name = "Peso",
                Shortened = "Kg"
            });

            Insert.IntoTable("MeasurementUnit").InSchema("Item").Row(new
            {
                Name = "Litro",
                Shortened = "L"
            });

            Insert.IntoTable("MeasurementUnit").InSchema("Item").Row(new
            {
                Name = "Comprimento",
                Shortened = "C"
            });

            Insert.IntoTable("MeasurementUnit").InSchema("Item").Row(new
            {
                Name = "Largura",
                Shortened = "L"
            });

            Insert.IntoTable("Category").InSchema("Item").Row(new
            {
                Name = "Construção"
            });

            Insert.IntoTable("Category").InSchema("Item").Row(new
            {
                Name = "Construção"
            });

            Insert.IntoTable("Category").InSchema("Item").Row(new
            {
                Name = "Tijolo de vidro",
                ParentCategoryId = 1
            });

            Insert.IntoTable("Microsite").InSchema("Item").Row(new
            {
                Name = "Construção",
                Active = false,
                Url = "/microsite/construcao",
                CategoryId = 1
        });

            Insert.IntoTable("Microsite").InSchema("Item").Row(new
            {
                Name = "Metalorgia",
                Active = false,
                Url = "mywaste/microsite/metalorgia",
                CategoryId = 2
            });

            Insert.IntoTable("Item").InSchema("Item").Row(new
            {
                Title = "Ferro",
                Price = 25,
                RegistrationDate = new DateTime(2017, 03, 13),
                Description = "Existem 55 unidades de ferro",
                Active = true,
                Highligthed = false,
                Quantity = 55,
                CategoryId = 1,
                CountyId = 1,
                CompanyId = 1,
                MeasurementUnitId = 1
            });

            Insert.IntoTable("Item").InSchema("Item").Row(new
            {
                Title = "Ferro",
                Price = 25,
                RegistrationDate = new DateTime(2017, 05, 20),
                Description = "Existem 3 unidades de ferro",
                Active = true,
                Highligthed = false,
                Quantity = 3,
                CategoryId = 2,
                CountyId = 2,
                CompanyId = 2,
                MeasurementUnitId = 2
            });

            Insert.IntoTable("File").InSchema("Item").Row(new
            {
                Url = "url-image-1",
                ItemId = 1
            });

            Insert.IntoTable("File").InSchema("Item").Row(new
            {
                Url = "url-image-2",
                ItemId = 2
            });

            // Subscription inserts
            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2016, 03, 29),
                EndDate = new DateTime(2017, 03, 29),
                Active = false,
                SubscriptionPlanId = 1,
                CompanyId = 1,
                MicrositeId = 1
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2017, 03, 29),
                EndDate = new DateTime(2019, 03, 29),
                Active = false,
                SubscriptionPlanId = 3,
                CompanyId = 1,
                MicrositeId = 1
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2019, 03, 29),
                EndDate = new DateTime(2020, 03, 29),
                Active = true,
                SubscriptionPlanId = 5,
                CompanyId = 1,
                MicrositeId = 1
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2015, 03, 29),
                EndDate = DateTime.MaxValue,
                Active = true,
                SubscriptionPlanId = 1,
                CompanyId = 2,
                MicrositeId = 2
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2018, 03, 29),
                EndDate = new DateTime(2018, 04, 29),
                Active = false,
                SubscriptionPlanId = 1,
                CompanyId = 3,
                MicrositeId = 2 
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2018, 04, 29),
                EndDate = new DateTime(2018, 05, 29),
                Active = false,
                SubscriptionPlanId = 2,
                CompanyId = 3,
                MicrositeId = 2
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2018, 05, 29),
                EndDate = new DateTime(2019, 05, 29),
                Active = false,
                SubscriptionPlanId = 5,
                CompanyId = 3,
                MicrositeId = 2
            });

            Insert.IntoTable("Subscription").InSchema("Subscription").Row(new
            {
                StartDate = new DateTime(2019, 05, 29),
                EndDate = new DateTime(2020, 05, 29),
                Active = true,
                SubscriptionPlanId = 5,
                CompanyId = 3,
                MicrositeId = 2
            });

            Insert.IntoTable("SubscriptionPayment").InSchema("Subscription").Row(new
            {
                Date = new DateTime(2017, 03, 29),
                AccountId = new Guid("3D7BC2DC-CC82-4B41-9550-143FD5FD17ED"),
                SubscriptionId = 2   ,
                InvoiceId = 1
            });

            Insert.IntoTable("SubscriptionPayment").InSchema("Subscription").Row(new
            {
                Date = new DateTime(2018, 03, 29),
                AccountId = new Guid("3D7BC2DC-CC82-4B41-9550-143FD5FD17ED"),
                SubscriptionId = 2,
                InvoiceId = 2
            });

            Insert.IntoTable("SubscriptionPayment").InSchema("Subscription").Row(new
            {
                Date = new DateTime(2019, 03, 29),
                AccountId = new Guid("3D7BC2DC-CC82-4B41-9550-143FD5FD17ED"),
                SubscriptionId = 3
            });

            Insert.IntoTable("SubscriptionPayment").InSchema("Subscription").Row(new
            {
                Date = new DateTime(2018, 04, 29),
                AccountId = new Guid("935B15E5-41C7-4513-9DDC-A4656EB2B566"),
                SubscriptionId = 6
            });

            Insert.IntoTable("SubscriptionPayment").InSchema("Subscription").Row(new
            {
                Date = new DateTime(2018, 05, 29),
                AccountId = new Guid("935B15E5-41C7-4513-9DDC-A4656EB2B566"),
                SubscriptionId = 7
            });

            Insert.IntoTable("SubscriptionPayment").InSchema("Subscription").Row(new
            {
                Date = new DateTime(2019, 05, 29),
                AccountId = new Guid("935B15E5-41C7-4513-9DDC-A4656EB2B566"),
                SubscriptionId = 8
            });
        }
    }
}