﻿using System;
using FluentMigrator;

namespace Infrastructure.Migrations.Tasks
{
    [Migration(3)]
    public class CreateSubscriptions : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema("Item").Table("County").Exists())
            {
                Create.Table("County").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Name").AsString(50).NotNullable();
            }

            if (!Schema.Schema("Item").Table("Company").Exists())
            {
                Create.Table("Company").InSchema("Item")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Name").AsString(150).NotNullable()
                    .WithColumn("Description").AsString(500).NotNullable()
                    .WithColumn("PhoneNumber").AsString(20).Nullable()
                    .WithColumn("MobilePhoneNumber").AsString(20).NotNullable()
                    .WithColumn("VATNumber").AsString(50).NotNullable()
                    .WithColumn("Address").AsString(200).NotNullable()
                    .WithColumn("AdditionalAddress").AsString(200).Nullable()
                    .WithColumn("PostalCode").AsString(20).NotNullable()
                    .WithColumn("CountyId").AsInt32();
            }

            if (!Schema.Schema("Subscription").Table("SubscriptionPlan").Exists())
            {
                Create.Table("SubscriptionPlan").InSchema("Subscription")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Name").AsString(70).NotNullable()
                    .WithColumn("Duration").AsInt32().NotNullable()
                    .WithColumn("Price").AsDouble().NotNullable()
                    .WithColumn("MonthlyContactRequestLimit").AsInt32().NotNullable()
                    .WithColumn("MonthlyItemHighlightLimit").AsInt32().NotNullable()
                    .WithColumn("NotificationsAvailable").AsBoolean().NotNullable();
            }

            if (!Schema.Schema("Subscription").Table("Subscription").Exists())
            {
                Create.Table("Subscription").InSchema("Subscription")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("StartDate").AsDateTime().NotNullable()
                    .WithColumn("EndDate").AsDateTime().NotNullable()
                    .WithColumn("Active").AsBoolean().NotNullable()
                    .WithColumn("SubscriptionPlanId").AsInt32().NotNullable()
                    .WithColumn("CompanyId").AsInt32().NotNullable()
                    .WithColumn("MicrositeId").AsInt32().NotNullable();
            }

            if (!Schema.Schema("Subscription").Table("SubscriptionPayment").Exists())
            {
                Create.Table("SubscriptionPayment").InSchema("Subscription")
                    .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                    .WithColumn("Date").AsDateTime().NotNullable()
                    .WithColumn("AccountId").AsGuid().NotNullable()
                    .WithColumn("SubscriptionId").AsInt32().NotNullable()
                    .WithColumn("InvoiceId").AsInt32().Nullable();
            }

            CreateForeignKeys();
            Seed();
        }

        public override void Down()
        {
            if (Schema.Table("SubscriptionPayment").Exists())
            {
                Delete.Table("SubscriptionPayment").InSchema("Subscription");
            }

            if (Schema.Table("Subscription").Exists())
            {
                Delete.Table("Subscription").InSchema("Subscription");
            }

            if (Schema.Table("SubscriptionPlan").Exists())
            {
                Delete.Table("SubscriptionPlan").InSchema("Subscription");
            }

            if (Schema.Table("Company").Exists())
            {
                Delete.Table("Company").InSchema("Item");
            }

            if (Schema.Table("County").Exists())
            {
                Delete.Table("County").InSchema("Item");
            } 
        }

        private void CreateForeignKeys()
        {
            // Account foreign key
            if (!Schema.Schema("Subscription").Table("Account").Constraint("FK_Account_Company").Exists())
            {
                Create.ForeignKey("FK_Account_Company")
                    .FromTable("Account").InSchema("Subscription").ForeignColumn("CompanyId")
                    .ToTable("Company").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Item").Table("Company").Constraint("FK_Company_County").Exists())
            {
                Create.ForeignKey("FK_Company_County")
                    .FromTable("Company").InSchema("Item").ForeignColumn("CountyId")
                    .ToTable("County").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Subscription").Table("Subscription").Constraint("FK_Subscription_SubscriptionPlan").Exists())
            {
                Create.ForeignKey("FK_Subscription_SubscriptionPlan")
                    .FromTable("Subscription").InSchema("Subscription").ForeignColumn("SubscriptionPlanId")
                    .ToTable("SubscriptionPlan").InSchema("Subscription").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Subscription").Table("Subscription").Constraint("FK_Subscription_Company").Exists())
            {
                Create.ForeignKey("FK_Subscription_Company")
                    .FromTable("Subscription").InSchema("Subscription").ForeignColumn("CompanyId")
                    .ToTable("Company").InSchema("Item").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Subscription").Table("SubscriptionPayment").Constraint("FK_SubscriptionPayment_Account").Exists())
            {
                Create.ForeignKey("FK_SubscriptionPayment_Account")
                    .FromTable("SubscriptionPayment").InSchema("Subscription").ForeignColumn("AccountId")
                    .ToTable("Account").InSchema("Subscription").PrimaryColumn("Id");
            }

            if (!Schema.Schema("Subscription").Table("SubscriptionPayment").Constraint("FK_SubscriptionPayment_Invoice").Exists())
            {
                Create.ForeignKey("FK_SubscriptionPayment_Invoice")
                    .FromTable("SubscriptionPayment").InSchema("Subscription").ForeignColumn("InvoiceId")
                    .ToTable("Invoice").InSchema("Subscription").PrimaryColumn("Id");
            }

        }

        private void Seed()
        {
            Insert.IntoTable("SubscriptionPlan").InSchema("Subscription").Row(new
            {
                Name = "Free",
                Duration = Int32.MaxValue,
                Price = 0,
                MonthlyContactRequestLimit = 20,
                MonthlyItemHighlightLimit = 5,
                NotificationsAvailable = false
            });
            
            Insert.IntoTable("SubscriptionPlan").InSchema("Subscription").Row(new
            {
                Name = "Standard",
                Duration = 365,
                Price = 100,
                MonthlyContactRequestLimit = 40,
                MonthlyItemHighlightLimit = 10,
                NotificationsAvailable = false
            });

            Insert.IntoTable("SubscriptionPlan").InSchema("Subscription").Row(new
            {
                Name = "Standard",
                Duration = 30,
                Price = 10,
                MonthlyContactRequestLimit = 40,
                MonthlyItemHighlightLimit = 10,
                NotificationsAvailable = false
            });

            Insert.IntoTable("SubscriptionPlan").InSchema("Subscription").Row(new
            {
                Name = "Premium",
                Duration = 365,
                Price = 200,
                MonthlyContactRequestLimit = 100,
                MonthlyItemHighlightLimit = 30,
                NotificationsAvailable = true
            });

            Insert.IntoTable("SubscriptionPlan").InSchema("Subscription").Row(new
            {
                Name = "Premium",
                Duration = 30,
                Price = 20,
                MonthlyContactRequestLimit = 100,
                MonthlyItemHighlightLimit = 30,
                NotificationsAvailable = true
            });

            Insert.IntoTable("County").InSchema("Item").Row(new
            {
                Name = "Setúbal"
            });

            Insert.IntoTable("County").InSchema("Item").Row(new
            {
                Name = "Lisboa"
            });

            Insert.IntoTable("Company").InSchema("Item").Row(new
            {
                Name = "InspireIT",
                Description = "Descrição da inspire",
                PhoneNumber = "939393939",
                MobilePhoneNumber = "969696969",
                VATNumber = "11961836712",
                Address = "Av. do Cristo Rei",
                PostalCode = "1231-123",
                CountyId = 1,
            });

            Insert.IntoTable("Company").InSchema("Item").Row(new
            {
                Name = "Pingo Doce",
                Description = "Venha cá",
                MobilePhoneNumber = "922222222",
                VATNumber = "312341231",
                Address = "Av. do Pingo Doce",
                PostalCode = "2975-310",
                CountyId = 1,
            });

            Insert.IntoTable("Company").InSchema("Item").Row(new
            {
                Name = "Continente",
                Description = "Vende coisas, muitas coisas, o que leva a uma descrição grande, muito grande, " +
                              "mesmo muito grande, tipo 500 caracteres percebes?" +
                              "Continente é uma insígnia de hipermercados pertencente à Sonae MC. " +
                              "Foi a primeira cadeia de hipermercados em Portugal, e mantém-se ainda hoje como" +
                              " uma referência no sector de retalho alimentar do país." +
                              "Todos os descontos e promoções estão aqui. Saiba como poupar em supermercado," +
                              " tecnologia, desporto, carro, decoração e muito mais." +
                              "Ainda faltam 40, pareco Fernando Pessoa",
                PhoneNumber = "944444444",
                MobilePhoneNumber = "956565656",
                VATNumber = "378565457",
                Address = "Av. dos aliados",
                PostalCode = "3124-131",
                CountyId = 2,
            });

            Insert.IntoTable("Account").InSchema("Subscription").Row(new
            {
                Id = new Guid("3D7BC2DC-CC82-4B41-9550-143FD5FD17ED"),
                FirstName = "Tiago",
                LastName = "Santos",
                CompanyId = 1
            });

            Insert.IntoTable("Account").InSchema("Subscription").Row(new
            {
                Id = new Guid("EB72481B-2020-406D-AB78-54F678AA18BD"),
                FirstName = "Hugo",
                LastName = "Ferreira",
                CompanyId = 2
            });

            Insert.IntoTable("Account").InSchema("Subscription").Row(new
            {
                Id = new Guid("935B15E5-41C7-4513-9DDC-A4656EB2B566"),
                FirstName = "Ines",
                LastName = "Norberto",
                CompanyId = 3
            });
        }
    }
}