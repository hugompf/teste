﻿using FluentMigrator;

namespace Infrastructure.Migrations.Tasks
{
    [Migration(1)]
    public class StartDatabase : Migration
    {
        public override void Up()
        {
            CreateSchemas();

            if (!Schema.Schema("Subscription").Table("Account").Exists())
            {
                Create.Table("Account").InSchema("Subscription")
                    .WithColumn("Id").AsGuid().PrimaryKey()
                    .WithColumn("FirstName").AsString(70).NotNullable()
                    .WithColumn("LastName").AsString(70).NotNullable()
                    .WithColumn("CompanyId").AsInt32().NotNullable();
            }
        }

        public override void Down()
        {
            if (Schema.Schema("Subscription").Table("Account").Exists())
            {
                Delete.Table("Account").InSchema("Subscription");
            }
        }

        private void CreateSchemas()
        {
            if (!Schema.Schema("Subscription").Exists())
            {
                Create.Schema("Subscription");
            }

            if (!Schema.Schema("Item").Exists())
            {
                Create.Schema("Item");
            }
        }
    }
}