﻿using FluentMigrator;

namespace Infrastructure.Migrations.Tasks
{
    [Migration(5)]
    public class TriggerOnSubscription : Migration
    {
        public override void Up()
        {
            CreateTriggers();
        }

        public override void Down()
        {
            DropTriggers();
        }

        private void CreateTriggers()
        {
            DropTriggers();

            Execute.Sql(@"CREATE TRIGGER Subscription_Insert_Error_Company_Has_Active_Subscription ON [Subscription].[Subscription] INSTEAD OF INSERT
                            AS
                            BEGIN
								DECLARE @companyId int;
								SELECT @companyId = CompanyId FROM INSERTED;

                                DECLARE @micrositeId int;
                                SELECT @micrositeId = MicrositeId FROM INSERTED;

								 IF EXISTS (SELECT * FROM [Subscription].Subscription WHERE Active = 1 AND CompanyId = @companyId AND MicrositeId = @micrositeId)
								 BEGIN
									DECLARE @errorMessage VARCHAR(200);
		                            SELECT @errorMessage = CONCAT('Company with id ', @companyId, ' already has an active subscription in this website.')
		                            RAISERROR(@errorMessage, 16, 1);
								 END
								 ELSE
								 BEGIN
		                            INSERT INTO [Subscription].Subscription (StartDate, EndDate, Active, SubscriptionPlanId, CompanyId, MicrositeId)
		                            SELECT StartDate, EndDate, Active, SubscriptionPlanId, CompanyId, MicrositeId
		                            FROM INSERTED;
	                            END
							END;");
        }

        private void DropTriggers()
        {
            Execute.Sql(@" IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[Subscription].Subscription_Insert_Error_Company_Has_Active_Subscription'))
                           DROP TRIGGER [Subscription].Subscription_Insert_Error_Company_Has_Active_Subscription");
        }
    }
}
