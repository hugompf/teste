import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({
  layout: Layout,
  component: Component,
  standalone = false,
  isAuthenticated = false,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        return isAuthenticated ? (
          Layout ? (
            <Layout>
              <Component {...props} />
            </Layout>
          ) : (
            <Component {...props} />
          )
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        );
      }}
    />
  );
};

const mapStateToProps = ({ auth }) => {
  return {
    isAuthenticated: auth.isAuthenticated,
  };
};

PrivateRoute.propTypes = {
  layout: PropTypes.func,
  component: PropTypes.func,
  standalone: PropTypes.bool,
  isAuthenticated: PropTypes.bool,
  location: PropTypes.object,
};

export default connect(
  mapStateToProps,
  null
)(PrivateRoute);
