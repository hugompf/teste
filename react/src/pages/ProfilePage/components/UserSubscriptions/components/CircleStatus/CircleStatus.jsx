import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Button';
// scss
import './CircleStatus.scss';

class CircleStatus extends Component {
  render() {
    const { active } = this.props;
    return (
      <Tooltip title={`${active ? `Ativo` : `Inativo`}`}>
        <span className={`${active ? `active` : `inactive`}`} />
      </Tooltip>
    );
  }
}

CircleStatus.propTypes = {
  active: PropTypes.bool,
};

export default CircleStatus;
