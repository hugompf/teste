import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Tooltip from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

// actions
import { fetchInvoice } from '../../../../../../store/actions/invoice.action';

// scss
import './InvoiceStatus.scss';

const StyledTooltip = withStyles({
  root: {
    padding: '0px',
    minWidth: '48px',
  },
})(Tooltip);

class InvoiceStatus extends Component {
  handleClick = async () => {
    await this.props.fetchInvoice(this.props.invoiceId);
    if (this.props.invoice) {
      const fileURL = URL.createObjectURL(this.props.invoice);
      window.open(fileURL);
    }
  };

  render() {
    const { invoiceId, status } = this.props;
    const tooltipText = `${
      invoiceId
        ? 'Download'
        : status === 'Free'
        ? 'Inexistente'
        : 'Indisponivel'
    }`;

    return (
      <StyledTooltip style={{}} title={tooltipText}>
        {invoiceId ? (
          <i
            className="large material-icons pointer"
            onClick={e => this.handleClick(e)}
          >
            cloud_download
          </i>
        ) : (
          <i className="large material-icons not-allowed">
            {' '}
            {status === 'Free' ? `not_interested` : `cloud_off`}
          </i>
        )}
      </StyledTooltip>
    );
  }
}

const mapStateToProps = ({ invoices }) => ({
  invoice: invoices && invoices.data,
  isInvoiceLoading: invoices && invoices.isInvoiceLoading,
  error: invoices && invoices.isInvoiceLoading,
});

const mapDispatchToProps = dispatch => ({
  fetchInvoice: id => dispatch(fetchInvoice(id)),
});

InvoiceStatus.propTypes = {
  invoice: PropTypes.object,
  fetchInvoice: PropTypes.func,
  invoiceId: PropTypes.number,
  status: PropTypes.string,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InvoiceStatus);
