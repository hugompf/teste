import React, { Component } from 'react';
import { connect } from 'react-redux';
import MUIDataTable from 'mui-datatables';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

//components
import CircleStatus from './components/CircleStatus/CircleStatus';
import InvoiceStatus from './components/InvoiceStatus/InvoiceStatus';
import PricingTable from '../../../../components/Pricing/PricingTable';

// scss
import './UserSubscriptions.scss';

//actions
import { fetchUserSubscriptions } from '../../../../store/actions/subscription.action';

class UserSubscriptions extends Component {
  constructor(props) {
    super(props);

    this.hasActiveSubscription = false;

    this.state = {
      data: [
        {
          id: 1,
          active: 1,
          plan_name: 'Premium',
          start_date: '12/12/12',
          end_date: '12/12/12',
          invoice_id: 1,
        },
        {
          id: 1,
          plan_name: 'Free',
          start_date: '12/12/12',
          end_date: '12/12/12',
          active: 1,
          invoice_id: 1,
        },
      ],
      count: 0,
      page: 0,
      pageSize: 10,
    };

    this.table = {
      columns: [
        {
          name: 'id',
          options: {
            display: 'excluded',
            filter: false,
          },
        },

        {
          name: 'Plano',
          options: {
            filter: true,
          },
        },
        {
          name: 'Início',
          options: {
            filter: false,
            customBodyRender: (value, tableMeta) => {
              return this.getDateFormat(value);
            },
          },
        },
        {
          name: 'Fim',
          options: {
            filter: false,
            customBodyRender: (value, tableMeta) => {
              return this.getDateFormat(value, tableMeta);
            },
          },
        },
        {
          name: 'Estado',
          options: {
            filter: false,
            customBodyRender: (value, tableMeta) => {
              return this.getActiveFormat(value, tableMeta);
            },
          },
        },
        {
          name: 'Fatura',
          options: {
            filter: false,
            sort: false,
            customBodyRender: (value, tableMeta) => {
              return this.getInvoiceFormat(value, tableMeta);
            },
          },
        },
      ],
      theme: createMuiTheme({
        typography: {
          fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif;',
          useNextVariants: true,
        },
        overrides: {
          MuiPaper: {
            elevation4: {
              boxShadow: 'none',
            },
          },
          MUIDataTableHeadCell: {
            root: {
              fontWeight: 'bold',
              color: '#000',
              fontSize: 15,
              padding: '10px',
              maxWidth: '50px',
              minWidth: '30px',
            },
          },
          MuiTableCell: {
            root: {
              height: '50px',
              padding: '0px !important',
              '@media (max-width: 767px)': {
                width: '40% !important',
              },
            },

            body: {
              fontSize: 13,
            },
          },
          MUIDataTableBodyCell: {
            root: {
              '&:nth-child(2)': {
                paddingLeft: '0.1em !important',
              },
            },
          },
          MuiToolbar: {
            root: {
              padding: '0px !important',
            },
          },
        },
      }),
    };
  }

  componentDidMount() {
    // this.props.fetchUserSubscriptions();
  }

  getDateFormat = (date, tableMeta) => {
    if (tableMeta && tableMeta.rowData[1] === 'Free') {
      if (tableMeta.rowData[4]) return '-----------------';
    }
    if (date) {
      return new Intl.DateTimeFormat('pt-PT').format(new Date(date));
    }
    return 'Não';
  };

  getActiveFormat = active => {
    return <CircleStatus active={active} />;
  };

  getInvoiceFormat = (invoice, tableMeta) => {
    if (invoice && tableMeta && tableMeta.rowData[5])
      return <InvoiceStatus invoiceId={tableMeta.rowData[5]} />;
    if (tableMeta) return <InvoiceStatus status={tableMeta.rowData[2]} />;
  };

  handleData = data => {
    const handledData = [];

    data.map(subscription => {
      if (subscription.active && !this.hasActiveSubscription) {
        this.hasActiveSubscription = true;
      }

      const subscriptionData = [];
      subscriptionData.push(
        subscription.id,
        subscription.active,
        subscription.plan_name,
        subscription.start_date,
        subscription.end_date,
        subscription.invoice_id
      );
      return handledData.push(subscriptionData);
    });

    return handledData;
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      const { data } = nextProps;
      const handledData = this.handleData(data.items);

      if (this.state.data !== handledData) {
        this.setState({
          data: handledData,
          count: data.total_rows,
          page: data.current_page,
          pageSize: data.rows_per_page,
        });
      }
    }
  }

  getOrder = tableState => {
    var order =
      tableState.announceText === null
        ? ''
        : tableState.announceText.includes('descending')
        ? 'DESC'
        : 'ASC';
    if (tableState.activeColumn === 1) return 'Active ' + order;
    else if (tableState.activeColumn === 2) return 'PlanName ' + order;
    else if (tableState.activeColumn === 3) return 'StartDate ' + order;
    else if (tableState.activeColumn === 4) return 'EndDate ' + order;
    else return 'StartDate DESC';
  };

  render() {
    const { data, count, page, pageSize } = this.state;
    const { columns, theme } = this.table;

    const options = {
      selectableRows: false,
      rowsPerPageOptions: [1, 3, 20],
      pagination: true,
      page: page - 1,
      count: count,
      rowsPerPage: pageSize,
      serverSide: true,
      fixedHeader: false,
      responsive: 'stacked',
      filter: false,
      print: false,
      onTableChange: (action, tableState) => {
        if (action !== 'onSearchOpen') {
          this.props.fetchUserSubscriptions(
            action === 'changePage' ? tableState.page + 1 : tableState.page,
            tableState.rowsPerPage,
            this.getOrder(tableState),
            tableState.searchText
          );
        }
      },
      download: false,
      textLabels: {
        body: {
          noMatch: 'Nenhuma subscrição encontrada',
          toolTip: 'Sort',
        },
        pagination: {
          next: 'Próxima página',
          previous: 'Página anterior',
          rowsPerPage: 'Resultados por página:',
          displayRows: 'de',
        },
        viewColumns: {
          title: 'Colunas',
          titleAria: 'Mostre/Esconda',
        },
        toolbar: {
          search: 'Procure',
          viewColumns: 'Ver colunas',
        },
      },
    };

    return (
      <div>
        <div className="subscription-table">
          <MuiThemeProvider theme={theme}>
            <MUIDataTable data={data} columns={columns} options={options} />
          </MuiThemeProvider>
        </div>
        {!this.hasActiveSubscription && <PricingTable />}
      </div>
    );
  }
}

const mapStateToProps = ({ subscriptions }) => ({
  data: subscriptions && subscriptions.data,
  error: subscriptions && subscriptions.error,
  isSubscriptionsLoading: subscriptions && subscriptions.isSubscriptionsLoading,
});

UserSubscriptions.propTypes = {
  fetchUserSubscriptions: PropTypes.func,
  data: PropTypes.object,
  error: PropTypes.string,
  isSubscriptionsLoading: PropTypes.bool,
};

export default connect(
  mapStateToProps,
  { fetchUserSubscriptions }
)(UserSubscriptions);
