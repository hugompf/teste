import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// import redux actions
import { login } from '../../store/actions/auth.action';

// scss
import './Login.scss';

class Login extends Component {
  render() {
    if (this.props.isAuthenticated) {
      const { from = '/' } = this.props.location.state || {};
      return <Redirect to={from} />;
    }
    return (
      <div className="login-page">
        Login page here <button onClick={this.props.login}>login</button>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  isAuthenticated: auth.isAuthenticated,
});

Login.propTypes = {
  login: PropTypes.func,
  isAuthenticated: PropTypes.bool,
  location: PropTypes.object,
};

export default connect(
  mapStateToProps,
  { login }
)(Login);
