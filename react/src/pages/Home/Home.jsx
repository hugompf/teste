import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// components
import SuperSpecificComponent from './components/SuperSpecificComponent';

// actions
import { setHomeHello } from './../../store/actions/home.action';

// scss
import './Home.scss';

class Home extends Component {
  componentDidMount() {
    this.props.setHomeHello();
  }

  render() {
    return (
      <div className="home">
        <h1>InspireIT Social</h1>
        <h2 className="home__hello">{this.props.hello}</h2>
        <SuperSpecificComponent />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hello: state.home && state.home.hello,
});

const mapDispatchToProps = dispatch => ({
  setHomeHello: () => dispatch(setHomeHello()),
});

Home.propTypes = {
  hello: PropTypes.string,

  setHomeHello: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
