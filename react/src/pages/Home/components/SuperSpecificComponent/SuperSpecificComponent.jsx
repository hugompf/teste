import React, { Component } from 'react';
// import PropTypes from 'prop-types';

// scss
import './SuperSpecificComponent.scss';

class SuperSpecificComponent extends Component {
  render() {
    return (
      <div className="super-specific-component">
        I am a really super specific component!
      </div>
    );
  }
}

SuperSpecificComponent.propTypes = {};

export default SuperSpecificComponent;
