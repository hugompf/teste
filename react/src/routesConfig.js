import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import LayoutNavbar from './components/LayoutNavbar';
import ProfilePage from './pages/ProfilePage/ProfilePage';

const routes = [
  { path: '/', exact: true, layout: LayoutNavbar, component: Home },
  {
    path: '/login',
    exact: true,
    component: Login,
  },
  {
    path: '/logout',
    exact: true,
    component: Logout,
  },
  {
    path: '/account',
    isPrivate: true,
    layout: LayoutNavbar,
    component: ProfilePage,
  },
];

export default routes;
