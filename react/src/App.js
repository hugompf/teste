import React, { Component, Fragment } from 'react';
import Router from './router';
import { ToastContainer } from 'react-toastify';

// toastr css
import 'react-toastify/dist/ReactToastify.css';

// components
import GlobalLoading from './components/GlobalLoading';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Router />
        <ToastContainer />
        <GlobalLoading />
      </Fragment>
    );
  }
}

export default App;
