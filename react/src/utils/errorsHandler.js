export async function handleErrors(response) {
  if (!response.ok) {
    let responseText = await response.text();
    const data = responseText && JSON.parse(responseText);
    const error =
      data[Object.keys(data)[0]][0] || data[0].message || response.statusText;
    return Promise.reject(error);
  }
}
