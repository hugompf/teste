export function createEndpoint(url, pageNumber, pageSize, order, filter) {
  var query = `${process.env.REACT_APP_MYW_CORE_API_URL}` + url;

  if (pageNumber) {
    query += '?pageNumber=' + pageNumber + '&';
  }
  if (pageSize) {
    query += getFirstElement(query) + 'pageSize=' + pageSize + '&';
  }
  if (order) {
    query += getFirstElement(query) + 'order=' + order + '&';
  }
  if (filter) {
    query += getFirstElement(query) + 'filter=' + filter;
  }
  return query;
}

function getFirstElement(query) {
  return query.substr(query.length - 1) === '&' ? '' : '?';
}
