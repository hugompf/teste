import { actions } from '../actions/invoice.action';

export default function invoices(state = null, action) {
  switch (action.type) {
    case actions.FETCH_INVOICE_SUCCESS:
      return { ...state, data: action.payload, isInvoiceLoading: false };
    case actions.FETCH_INVOICE_FAILURE:
      return { ...state, error: action.error, isInvoiceLoading: false };
    case actions.FETCH_INVOICE_REQUEST:
      return { ...state, isInvoiceLoading: true };
    default:
      return state;
  }
}
