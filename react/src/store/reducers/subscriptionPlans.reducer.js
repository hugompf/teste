import { actions } from '../actions/subscriptionPlans.action';

export default function subscriptionPlans(state = null, action) {
  switch (action.type) {
    case actions.FETCH_ALL_SUBSCRIPTIONS_SUCCESS:
      return { ...state, data: action.payload, isSubscriptionsLoading: false };
    case actions.FETCH_ALL_SUBSCRIPTIONS_FAILURE:
      return { ...state, error: action.error, isSubscriptionsLoading: false };
    case actions.FETCH_ALL_SUBSCRIPTIONS_REQUEST:
      return { ...state, isSubscriptionsLoading: true };
    default:
      return state;
  }
}
