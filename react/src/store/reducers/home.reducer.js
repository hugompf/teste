import { actions } from '../actions/home.action';

export default function home(state = null, action) {
  switch (action.type) {
    case actions.SET_HOME_HELLO:
      return {
        ...state,
        hello: action.payload,
      };
    default:
      return state;
  }
}
