import { actions } from '../actions/someOther.action';

export default function home(state = null, action) {
  switch (action.type) {
    case actions.FETCH_SOME_OTHER_DATA:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}
