import auth from './auth.reducer';
import home from './home.reducer';
import someOther from './someOther.reducer';
import invoices from './invoice.reducer';
import subscriptions from './subscription.reducer';
import globalLoading from './globalLoading.reducer';
import subscriptionPlans from './subscriptionPlans.reducer';

export default {
  auth,
  home,
  someOther,
  invoices,
  subscriptions,
  globalLoading,
  subscriptionPlans,
};
