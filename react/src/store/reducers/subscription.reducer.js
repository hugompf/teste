import { actions } from '../actions/subscription.action';

export default function subscriptions(state = null, action) {
  switch (action.type) {
    case actions.FETCH_USER_SUBSCRIPTIONS_SUCCESS:
      return { ...state, data: action.payload, isSubscriptionsLoading: false };
    case actions.FETCH_USER_SUBSCRIPTIONS_FAILURE:
      return { ...state, error: action.error, isSubscriptionsLoading: false };
    case actions.FETCH_USER_SUBSCRIPTIONS_REQUEST:
      return { ...state, isSubscriptionsLoading: true };
    default:
      return state;
  }
}
