import { actions } from '../actions/auth.action';

const defaultState = {
  isAuthenticated: false,
};

export default function home(state = defaultState, action) {
  switch (action.type) {
    case actions.AUTH:
      return { ...state, isAuthenticated: true };
    case actions.LOGOUT:
      return { ...state, isAuthenticated: false };
    default:
      return state;
  }
}
