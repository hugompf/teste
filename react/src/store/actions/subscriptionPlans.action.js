import { handleErrors } from '../../utils/errorsHandler';

export const actions = {
  FETCH_ALL_SUBSCRIPTIONS_FAILURE: 'FETCH_ALL_SUBSCRIPTIONS_FAILURE',
  FETCH_ALL_SUBSCRIPTIONS_SUCCESS: 'FETCH_ALL_SUBSCRIPTIONS_SUCCESS',
  FETCH_ALL_SUBSCRIPTIONS_REQUEST: 'FETCH_ALL_SUBSCRIPTIONS_REQUEST',
};

export const fetchAllSubscriptions = () => async dispatch => {
  dispatch(fetchAllSubscriptionsRequest());

  try {
    let payload = await fetch(
      `${process.env.REACT_APP_MYW_CORE_API_URL}/api/subscription/all`,
      {
        method: 'GET',
      }
    );

    await handleErrors(payload);
    let subscriptions = await payload.json();

    dispatch(fetchAllSubscriptionsSuccess(subscriptions));
  } catch (error) {
    dispatch(fetchAllSubscriptionsFailure(error));
  }

  function fetchAllSubscriptionsRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ALL_SUBSCRIPTIONS_REQUEST,
      });
    };
  }

  function fetchAllSubscriptionsSuccess(subscriptions) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ALL_SUBSCRIPTIONS_SUCCESS,
        payload: subscriptions,
      });
    };
  }

  function fetchAllSubscriptionsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ALL_SUBSCRIPTIONS_FAILURE,
        error,
      });
    };
  }
};
