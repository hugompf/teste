export const actions = {
  FETCH_SOME_OTHER_DATA: 'FETCH_SOME_OTHER_DATA',
};

export const fetchSomeOtherData = () => async dispatch => {
  let payload = await fetch('https://jsonplaceholder.typicode.com/users');
  payload = await payload.json();
  dispatch({
    type: actions.FETCH_SOME_OTHER_DATA,
    payload,
  });
};
