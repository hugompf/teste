export const actions = {
  SET_HOME_HELLO: 'SET_HOME_HELLO',
};

export function setHomeHello() {
  return dispatch => {
    const helloArray = ['Hey!', 'Hello!', 'Oi!', 'Boas!', 'Yarr!', 'Hiho!'];
    const randomHelloArrayValue =
      helloArray[Math.floor(Math.random() * helloArray.length)];

    dispatch({
      type: actions.SET_HOME_HELLO,
      payload: randomHelloArrayValue,
    });
  };
}
