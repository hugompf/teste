export const actions = {
  AUTH: 'AUTH',
  LOGOUT: 'LOGOUT',
};

export const login = () => dispatch => {
  dispatch({
    type: actions.AUTH,
    payload: true,
  });
};

export const logout = () => dispatch => {
  dispatch({
    type: actions.LOGOUT,
    payload: true,
  });
};
