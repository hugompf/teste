import { handleErrors } from '../../utils/errorsHandler';
import { notifyError } from '../../utils/notifications';

export const actions = {
  FETCH_INVOICE_SUCCESS: 'DOWNLOAD_INVOICE_SUCCESS',
  FETCH_INVOICE_REQUEST: 'DOWNLOAD_INVOICE_REQUEST',
  FETCH_INVOICE_FAILURE: 'DOWNLOAD_INVOICE_FAILURE',
};

export const fetchInvoice = id => async dispatch => {
  dispatch(fetchInvoiceRequest());
  try {
    var payload = await fetch(
      `${process.env.REACT_APP_MYW_CORE_API_URL}/api/invoices/` + id,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/pdf',
        },
      }
    );

    await handleErrors(payload);

    let response = await payload.blob();
    const pdfFile = new Blob([response], {
      type: 'application/pdf',
    });

    dispatch(fetchInvoiceSuccess(pdfFile));
  } catch (error) {
    dispatch(fetchInvoiceFailure(error));
  }

  function fetchInvoiceRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_INVOICE_REQUEST,
      });
    };
  }

  function fetchInvoiceFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_INVOICE_FAILURE,
        error,
      });
      notifyError(error);
    };
  }

  function fetchInvoiceSuccess(pdfFile) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_INVOICE_SUCCESS,
        payload: pdfFile,
      });
    };
  }
};
