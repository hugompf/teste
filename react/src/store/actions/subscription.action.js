import { handleErrors } from '../../utils/errorsHandler';
import { createEndpoint } from '../../utils/pagination';
import { notifyError } from './../../utils/notifications';

export const actions = {
  FETCH_USER_SUBSCRIPTIONS_FAILURE: 'FETCH_USER_SUBSCRIPTIONS_FAILURE',
  FETCH_USER_SUBSCRIPTIONS_SUCCESS: 'FETCH_USER_SUBSCRIPTIONS_SUCCESS',
  FETCH_USER_SUBSCRIPTIONS_REQUEST: 'FETCH_USER_SUBSCRIPTIONS_REQUEST',
};

export const fetchUserSubscriptions = (
  pageNumber,
  pageSize,
  order,
  filter
) => async dispatch => {
  dispatch(fetchUserSubscriptionsRequest());
  try {
    let payload = await fetch(
      createEndpoint(
        '/api/subscription/my-subscriptions',
        pageNumber,
        pageSize,
        order,
        filter
      ),
      {
        method: 'GET',
      }
    );

    await handleErrors(payload);
    let subscriptions = await payload.json();
    dispatch(fetchUserSubscriptionsSuccess(subscriptions));
  } catch (error) {
    dispatch(
      fetchUserSubscriptionsFailure(
        error instanceof Object ? error.message : error
      )
    );
  }

  function fetchUserSubscriptionsRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_USER_SUBSCRIPTIONS_REQUEST,
      });
    };
  }

  function fetchUserSubscriptionsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_USER_SUBSCRIPTIONS_FAILURE,
        error,
      });
      notifyError(
        error === 'Failed to fetch' ? 'Servidor indisponível' : error
      );
    };
  }

  function fetchUserSubscriptionsSuccess(subscriptions) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_USER_SUBSCRIPTIONS_SUCCESS,
        payload: subscriptions,
      });
    };
  }
};
