import React from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';

import { fetchAllSubscriptions } from '../../store/actions/subscriptionPlans.action';

//components
import RegularButton from '../Buttons/Button';
import PricingCard from './components/PricingCard/PricingCard';

import Durations from './components/PricingCard/card-durations';

// scss
import './PricingTable.scss';

class PricingTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      monthly: true,
      selectedButton: 1,
    };
  }

  componentDidMount() {
    this.props.fetchAllSubscriptions();
  }

  buildDurationSelectButtons() {
    var buttons = {};
    var selected = {};

    if (this.state.selectedButton === Durations.annual) {
      selected.monthly = 'multiUnselected';
      selected.annual = 'multiSelected';
    } else {
      selected.monthly = 'multiSelected';
      selected.annual = 'multiUnselected';
    }

    buttons.monthlyButton = (
      <div className="monthly-button-container">
        <RegularButton
          type="button"
          onClick={e => this.setSubscriptionsToMonthly(e)}
          simple
          className={'monthly-button'}
          color={selected.monthly}
        >
          Mensal
        </RegularButton>
      </div>
    );

    buttons.annualButton = (
      <div className="annual-button-container">
        <RegularButton
          type="button"
          onClick={e => this.setSubscriptionsToAnnual(e)}
          simple
          className={'annual-button'}
          color={selected.annual}
        >
          Anual
        </RegularButton>
      </div>
    );

    return buttons;
  }

  handleData = data => {
    const handledData = [];

    data.map(subscription => {
      return handledData.push(subscription);
    });

    return handledData;
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      const { data } = nextProps;

      const handledData = this.handleData(data);

      if (this.state.data !== handledData) {
        this.setState({ data: handledData });
      }
    }
  }

  getMonthlySubscriptions() {
    return this.getSubscriptionList(true);
  }

  getAnnualSubscriptions() {
    return this.getSubscriptionList(false);
  }

  getSubscriptionList(monthly) {
    let monthlyDuration = 30;
    let subs = this.state.data;

    if (monthly) {
      let monthlySubs = subs.filter(e => e.duration <= monthlyDuration);
      monthlySubs.unshift(subs.filter(e => e.price === 0)[0]);

      return monthlySubs;
    } else {
      return subs.filter(e => e.duration > monthlyDuration);
    }
  }

  setMonthlyState(value) {
    let duration = value ? Durations.monthly : Durations.annual;

    let monthly = { ...this.state.monthly };
    monthly = value;
    this.setState({ monthly, selectedButton: duration });
  }

  setSubscriptionsToMonthly = () => {
    this.setMonthlyState(true);
  };

  setSubscriptionsToAnnual = () => {
    this.setMonthlyState(false);
  };

  render() {
    const { isSubscriptionsLoading, error } = this.props;

    if (isSubscriptionsLoading) {
      return <h3 className="default-text">Loading</h3>;
    }

    if (this.state.data.length) {
      let list = this.state.monthly
        ? this.getMonthlySubscriptions()
        : this.getAnnualSubscriptions();

      let buttons = this.buildDurationSelectButtons();

      let cards = list.map(e => (
        <PricingCard
          key={e.name}
          duration={this.state.monthly ? Durations.monthly : Durations.annual}
          title={e.name}
          price={e.price}
          contactLimit={e.monthly_contact_request_limit}
          highlightLimit={e.monthly_item_highlight_limit}
          notificationsAvailable={e.notifications_available}
        />
      ));

      return (
        <div className="pricing">
          <div className="pricing-table-buttons">
            {buttons.monthlyButton}
            {buttons.annualButton}
          </div>
          <div className="pricing-table">{cards}</div>
        </div>
      );
    } else {
      return <h3 className="default-text"> {error} </h3>;
    }
  }
}

const mapStateToProps = ({ subscriptionPlans }) => ({
  isSubscriptionsLoading:
    subscriptionPlans && subscriptionPlans.isSubscriptionLoading,
  data: subscriptionPlans && subscriptionPlans.data,
  error: subscriptionPlans && subscriptionPlans.error,
});

PricingTable.propTypes = {
  fetchAllSubscriptions: PropTypes.func,
  buttons: PropTypes.object,
  data: PropTypes.array,
  error: PropTypes.string,
  isSubscriptionsLoading: PropTypes.bool,
};

export default connect(
  mapStateToProps,
  { fetchAllSubscriptions }
)(PricingTable);
