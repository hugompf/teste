import React from 'react';
import PropTypes from 'prop-types';

// scss
import './PricingCard.scss';

//components
import RegularButton from '../../../../components/Buttons/Button';

import Durations from './card-durations';

class PricingCard extends React.Component {
  buttonAction() {
    console.log('Button pressed');
  }

  setDurationVariables(duration, limit) {
    return {
      durationLabel: duration,
      limitLabel: limit,
    };
  }

  render() {
    const {
      duration,
      title,
      price,
      contactLimit,
      highlightLimit,
      notificationsAvailable,
    } = this.props;

    let durationVariables;

    if (duration === Durations.monthly) {
      durationVariables = this.setDurationVariables('/mês', 'mensais');
    } else if (duration === Durations.annual) {
      durationVariables = this.setDurationVariables('/ano', 'anuais');
    } else {
      durationVariables = this.setDurationVariables('--', '--');
    }

    let notification;

    if (notificationsAvailable) {
      notification = (
        <li>
          <i className="material-icons notification check">check</i>
          <span> Notificações ativas</span>
        </li>
      );
    } else {
      notification = (
        <li>
          <i className="material-icons notification close">close</i>
          <span> Notificações desativadas</span>
        </li>
      );
    }

    return (
      <div className="pricing-card">
        <h2 className="pricing-text-title">{title}</h2>
        <br />
        <div className="pricing-amount">
          <h1 className="pricing-text-amount">{price}</h1>
          <h2 className="pricing-text-currency">€</h2>
        </div>
        <h3 className="pricing-text-duration">
          {durationVariables.durationLabel}
        </h3>
        <h3 className="pricing-text-limit-title">
          Limites {durationVariables.limitLabel}
        </h3>
        <ul className="pricing-text-list">
          <li>
            <h4 className="pricing-text-limit">{contactLimit}</h4> Pedidos de
            contacto
          </li>
          <li>
            <h4 className="pricing-text-limit">{highlightLimit}</h4> Produtos
            destacados
          </li>
          {notification}
        </ul>
        <RegularButton
          type="button"
          onClick={this.buttonAction}
          round
          className={'pricing-button'}
          color={'primary'}
        >
          Aderir
        </RegularButton>
      </div>
    );
  }
}

PricingCard.propTypes = {
  duration: PropTypes.oneOf(Object.values(Durations)),
  title: PropTypes.string,
  price: PropTypes.number,
  contactLimit: PropTypes.number,
  highlightLimit: PropTypes.number,
  notificationsAvailable: PropTypes.bool,
};

export default PricingCard;
