import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
//import { Link } from 'react-router-dom';
import Header from './components/Header/Header';
import HeaderLinks from './components/Header/HeaderLinks';
import navbarStyle from '../../assets/jss/material-kit-react/navbarStyles';

// actions
import { setHomeHello } from './../../store/actions/home.action';

import withStyles from '@material-ui/core/styles/withStyles';

// the last import should always be the scss
import './Navbar.scss';
class Navbar extends Component {
  render() {
    const { classes, ...rest } = this.props;

    return (
      <Header
        brand="MyWaste"
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 250,
          color: 'white',
        }}
        rightLinks={
          <HeaderLinks isAuthenticated={this.props.isAuthenticated} />
        }
        {...rest}
      />
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  isAuthenticated: auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  setHomeHello: () => dispatch(setHomeHello()),
});

Navbar.propTypes = {
  isAuthenticated: PropTypes.bool,

  setHomeHello: PropTypes.func,
};

export default withStyles(navbarStyle)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Navbar)
);
