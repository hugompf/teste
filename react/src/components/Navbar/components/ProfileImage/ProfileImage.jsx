import React from 'react';
import PropTypes from 'prop-types';

import NotificationCircle from '../../../NotificationCircle/NotificationCircle';

import './ProfileImage.scss';

class ProfileImage extends React.Component {
  render() {
    const { notificationNumber } = this.props;

    var notificationClass = 'notification-circle-profile-small';

    if (notificationNumber >= 100) {
      notificationClass = 'notification-circle-profile-big';
    }

    return (
      <div className="profile-image-container">
        <img
          src={
            'https://scontent.flis8-1.fna.fbcdn.net/v/t1.0-1/25552047_1947195808642171_902642914618070113_n.jpg?_nc_cat=103&_nc_ht=scontent.flis8-1.fna&oh=ed4cb9269e68b4a6f444be9b0635b5cd&oe=5D43A2F3'
          }
          className="profile-image"
          alt="perfil"
        />
        <NotificationCircle
          notificationNumber={notificationNumber}
          circleClassName={notificationClass}
        />
      </div>
    );
  }
}

ProfileImage.propTypes = {
  notificationNumber: PropTypes.number,
};

export default ProfileImage;
