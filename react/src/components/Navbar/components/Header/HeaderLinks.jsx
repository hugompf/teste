import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

// core components
import Button from '../../../Buttons/Button';
import CustomDropdown from '../../../CustomDropdown/CustomDropdown';
import ProfileImage from '../ProfileImage/ProfileImage';
import DropdownButton from '../DropdownButton/DropdownButton';

import headerLinksStyle from '../../../../assets/jss/material-kit-react/headerLinksStyle';

function HeaderLinks({ isAuthenticated, ...props }) {
  const { classes } = props;

  const wishlistNotifications = 12;

  // Notifications in the notification button
  const notificationNotifications = 6;

  const totalNotifications = wishlistNotifications + notificationNotifications;

  return (
    <List className={classes.list}>
      {isAuthenticated ? (
        <div style={{ display: 'inline' }}>
          <ListItem className={classes.listItem}>
            <Button
              href="/catalog"
              color="transparent"
              className={classes.navLink}
            >
              Catálogo
            </Button>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Button
              href="/news"
              color="transparent"
              className={classes.navLink}
            >
              Notícias
            </Button>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Button
              key="contact"
              href="/contact"
              color="transparent"
              className={classes.navLink}
            >
              Contacta-nos
            </Button>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Button
              key="about"
              href="/about"
              color="transparent"
              className={classes.navLink}
            >
              Acerca de nós
            </Button>
          </ListItem>

          <ListItem className={classes.listItem}>
            <CustomDropdown
              right
              caret={false}
              hoverColor="black"
              buttonText={
                <ProfileImage notificationNumber={totalNotifications} />
              }
              buttonProps={{
                className: classes.navLink + ' ' + classes.imageDropdownButton,
                color: 'transparent',
              }}
              noLiPadding
              dropdownList={[
                <DropdownButton
                  key="profile"
                  href="/account"
                  text="Perfil"
                  iconImage="person"
                />,
                <DropdownButton
                  key="wishlist"
                  href="/wishlist"
                  text="Wishlist"
                  iconImage="favorite"
                  notificationNumber={wishlistNotifications}
                />,
                <DropdownButton
                  key="notifications"
                  href="/notifications"
                  text="Notificações"
                  iconImage="notifications"
                  notificationNumber={notificationNotifications}
                />,
                <DropdownButton
                  key="logout"
                  href="/logout"
                  text="Terminar sessão"
                  iconImage="arrow_back"
                />,
                { divider: true },
                <DropdownButton
                  key="login"
                  href="/login"
                  text="Termos e condições"
                />,
                <DropdownButton key="faq" href="/faq" text="FAQ" />,
              ]}
            />
          </ListItem>
        </div>
      ) : (
        <div style={{ display: 'inline' }}>
          <ListItem className={classes.listItem}>
            <Button
              href="/login"
              color="transparent"
              className={classes.navLink}
            >
              Login
            </Button>
          </ListItem>
          <ListItem className={classes.listItem}>
            <Button
              href="/logout"
              color="transparent"
              className={classes.navLink}
            >
              Logout
            </Button>
          </ListItem>
        </div>
      )}
    </List>
  );
}

HeaderLinks.propTypes = {
  isAuthenticated: PropTypes.bool,
};

export default withStyles(headerLinksStyle)(HeaderLinks);
