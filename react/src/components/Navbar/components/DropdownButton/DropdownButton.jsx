import React from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import Icon from '@material-ui/core/Icon';

import Button from '../../../Buttons/Button';
import NotificationCircle from '../../../NotificationCircle/NotificationCircle';
import buttonStyle from '../../../../assets/jss/material-kit-react/buttonStyle';

import './DropdownButton.scss';

class DropdownButton extends React.Component {
  render() {
    const {
      classes,
      key,
      href,
      color,
      text,
      iconImage,
      notificationNumber,
    } = this.props;

    var icon;

    if (iconImage && notificationNumber) {
      icon = (
        <div className="dropdown-notification-container">
          <Icon className="dropdown-button-icon">{iconImage}</Icon>
          <NotificationCircle
            notificationNumber={notificationNumber}
            circleClassName="notification-circle-dropdown"
          />
        </div>
      );
    } else if (iconImage) {
      icon = <Icon className="dropdown-button-icon">{iconImage}</Icon>;
    }

    return (
      <div className="dropdown-button-container">
        <Button
          key={key}
          href={href}
          color={color}
          className={classes.dropdownButton}
        >
          <div className="dropdown-button-content-container">
            {icon}
            {text}
          </div>
        </Button>
      </div>
    );
  }
}

DropdownButton.defaultProps = {
  color: 'transparent',
};

DropdownButton.propTypes = {
  classes: PropTypes.object,
  key: PropTypes.string,
  href: PropTypes.string,
  color: PropTypes.string,
  text: PropTypes.string,
  iconImage: PropTypes.string,
  notificationNumber: PropTypes.number,
};

export default withStyles(buttonStyle)(DropdownButton);
