import React from 'react';
import PropTypes from 'prop-types';
import Navbar from '../Navbar';

const Container = ({ children }) => {
  return (
    <div className="app">
      <Navbar />
      <div className="app__content">{children}</div>
    </div>
  );
};

Container.propTypes = {
  children: PropTypes.any,
};

export default Container;
