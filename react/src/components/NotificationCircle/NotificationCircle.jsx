import React from 'react';
import PropTypes from 'prop-types';

import './NotificationCircle.scss';

class NotificationCircle extends React.Component {
  render() {
    const { notificationNumber, circleClassName } = this.props;

    return <span className={circleClassName}>{notificationNumber}</span>;
  }
}

NotificationCircle.defaultProps = {
  circleClassName: 'notification-circle-profile-small',
};

NotificationCircle.propTypes = {
  notificationNumber: PropTypes.number,
  circleClassName: PropTypes.string,
};

export default NotificationCircle;
