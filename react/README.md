# InspireIT Boilerplate
This is a boilerplate meant for InspireIT projects.

### Included Packages
In this boilerplate you can find the following packages:

- [**node-sass**](https://github.com/sass/node-sass) - Use .scss files instead of .css
- [**react-router-dom**](https://github.com/ReactTraining/react-router#readme) - Routing
- [**redux**](https://github.com/reduxjs/redux) - State management
- [**react-redux**](https://github.com/reduxjs/react-redux) - React bindings for Redux (mapStateToProps, mapDispatchToProps)
- [**redux-thunk**](https://github.com/reduxjs/redux-thunk) - Use actions asynchronously through dispatch and access the state easily

### Versioning
Whenever modifying this boilerplate project you **must** increment the minor (0.0.x) version of the package.json. 
 
### Folder Structure
In this section the different folders will be explained.

#### assets
This folder is where the _shared.scss file lives, which is meant for all the generic scss variables (and imports).

#### store
This folder is where the actions and reducers will be located at.

#### pages
This folder is meant for the different pages of the web app.

There might be cases where some components are specific to the page, in this case you should make another Components folder inside the page folder you're working at.

#### components
This folder is where all the React components which are generic to the application should be at.

If for example you have a Modal which is meant to be used for all the pop-ups (such as deleting a contact or alerting the user) it should be located here.

#### router
In this folder we have the mechanism for both the private and public routing.

The configuration is one through the routesConfig.js file which should be
carefully analyzed.

The most important properties are:

- **layout** - The layout the route will be inserted in. This can be used to
make both a page without any type of navigation, such as the login page,
and a page that has one, like the home page;
- **isPrivate** - Whether you need authentication to use this route.

### .env
Here are all the necessary variables that your project might need.

In this project you will find a .env.example that is because .env should always
be ignored from GIT and as such you must copy paste the data from .env.example
to your .env file so that SCSS imports can start the path from the src file.

### package.json
As a way to keep track of the version of the project you should keep incrementing
the version of the project with each commit, incrementing the minor version, and only
the medium and major when the project is getting a dramatic upgrade.

### SCSS
We heavily encourage reading [BEM](http://getbem.com/) before starting any SCSS
because it is how the project should be structured CSS-wise. Although it is not
the easiest convention to follow, with practice it gives a solid ground on being
able to better think of how to organize CSS and specially how to keep a naming
convention that makes sense.

### Visual Studio Code
For those who use visual studio code you should have the following configuration:
```
{
  "editor.tabSize": 2,
  "editor.rulers": [
    80,
    120
  ],
  "eslint.enable": true,
  "eslint.autoFixOnSave": true,
  "eslint.alwaysShowStatus": true,
}
```